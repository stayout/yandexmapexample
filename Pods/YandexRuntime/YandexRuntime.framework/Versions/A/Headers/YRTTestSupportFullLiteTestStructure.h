#import <YandexRuntime/YRTTestSupportTestTypes.h>

#import <UIKit/UIKit.h>

/// @cond EXCLUDE
@interface YRTTestSupportFullLiteTestStructure : NSObject

@property (nonatomic, readonly) BOOL b;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *ob;

@property (nonatomic, readonly) NSInteger i;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *oi;

@property (nonatomic, readonly) NSUInteger ui;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *oui;

@property (nonatomic, readonly) long long i64;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *oi64;

@property (nonatomic, readonly) float fl;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *ofl;

@property (nonatomic, readonly) double d;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *od;

@property (nonatomic, readonly, nonnull) NSString *s;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *os;

@property (nonatomic, readonly) NSTimeInterval ti;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *oti;

@property (nonatomic, readonly, nonnull) NSDate *at;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSDate *oat;

@property (nonatomic, readonly, nonnull) NSDate *rt;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSDate *ort;

@property (nonatomic, readonly, nonnull) NSData *by;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSData *oby;

@property (nonatomic, readonly, nonnull) UIColor *c;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) UIColor *oc;

@property (nonatomic, readonly) CGPoint p;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSValue *op;

@property (nonatomic, readonly) YRTTestSupportTestEnum e;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *oe;

@property (nonatomic, readonly) YRTTestSupportTestBitfieldEnum be;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *obe;


+ (nonnull YRTTestSupportFullLiteTestStructure *)fullLiteTestStructureWithB:( BOOL)b
                                                                         ob:(nullable NSNumber *)ob
                                                                          i:( NSInteger)i
                                                                         oi:(nullable NSNumber *)oi
                                                                         ui:( NSUInteger)ui
                                                                        oui:(nullable NSNumber *)oui
                                                                        i64:( long long)i64
                                                                       oi64:(nullable NSNumber *)oi64
                                                                         fl:( float)fl
                                                                        ofl:(nullable NSNumber *)ofl
                                                                          d:( double)d
                                                                         od:(nullable NSNumber *)od
                                                                          s:(nonnull NSString *)s
                                                                         os:(nullable NSString *)os
                                                                         ti:( NSTimeInterval)ti
                                                                        oti:(nullable NSNumber *)oti
                                                                         at:(nonnull NSDate *)at
                                                                        oat:(nullable NSDate *)oat
                                                                         rt:(nonnull NSDate *)rt
                                                                        ort:(nullable NSDate *)ort
                                                                         by:(nonnull NSData *)by
                                                                        oby:(nullable NSData *)oby
                                                                          c:(nonnull UIColor *)c
                                                                         oc:(nullable UIColor *)oc
                                                                          p:( CGPoint)p
                                                                         op:(nullable NSValue *)op
                                                                          e:( YRTTestSupportTestEnum)e
                                                                         oe:(nullable NSNumber *)oe
                                                                         be:( YRTTestSupportTestBitfieldEnum)be
                                                                        obe:(nullable NSNumber *)obe;


@end
/// @endcond

