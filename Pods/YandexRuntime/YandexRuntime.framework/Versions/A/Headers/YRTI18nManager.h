#import <YandexRuntime/YRTCanonicalUnit.h>
#import <YandexRuntime/YRTI18nPrefs.h>
#import <YandexRuntime/YRTPlatformBinding.h>
#import <YandexRuntime/YRTPrefs.h>

typedef void(^YRTLocaleUpdateDelegate)(
    NSError *error);

typedef void(^YRTLocaleResetDelegate)(
    NSError *error);

typedef void(^YRTLocaleDelegate)(
    NSString *locale,
    NSError *error);

@interface YRTI18nManager : YRTPlatformBinding

- (nonnull NSString *)localizeDistanceWithDistance:(NSInteger)distance;


- (nonnull NSString *)localizeDurationWithDuration:(NSInteger)duration;


- (nonnull NSString *)localizeSpeedWithSpeed:(double)speed;


- (nonnull NSString *)localizeDataSizeWithDataSize:(long long)dataSize;


- (nonnull YRTCanonicalUnit *)canonicalSpeedWithSpeed:(double)speed;


- (nonnull NSString *)localizeCanonicalUnitWithCanonicalUnit:(nonnull YRTCanonicalUnit *)canonicalUnit;


@property (nonatomic, nonnull) YRTI18nPrefs *prefs;

@property (nonatomic) YRTSystemOfMeasurement som;

@property (nonatomic) YRTTimeFormat timeFormat;

/**
 * Tells if this object is valid or no. Any method called on an invalid
 * object will throw an exception. The object becomes invalid only on UI
 * thread, and only when its implementation depends on objects already
 * destroyed by now. Please refer to general docs about the interface for
 * details on its invalidation.
 */
@property (nonatomic, readonly, getter=isValid) BOOL valid;

@end

