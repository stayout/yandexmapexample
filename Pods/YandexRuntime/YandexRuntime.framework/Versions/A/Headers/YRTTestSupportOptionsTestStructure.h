#import <Foundation/Foundation.h>

@interface YRTTestSupportOptionsTestStructure : NSObject

/**
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSNumber *b;

/**
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSString *text;

/**
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSNumber *interval;

/**
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSDate *timestamp;

+ (nonnull YRTTestSupportOptionsTestStructure *)optionsTestStructureWithB:(nullable NSNumber *)b
                                                                     text:(nullable NSString *)text
                                                                 interval:(nullable NSNumber *)interval
                                                                timestamp:(nullable NSDate *)timestamp;


@end
