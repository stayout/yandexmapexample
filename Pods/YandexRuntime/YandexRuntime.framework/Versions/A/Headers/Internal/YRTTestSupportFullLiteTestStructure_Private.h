#import <YandexRuntime/YRTTestSupportFullLiteTestStructure.h>

#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>
#import <yandex/maps/runtime/internal/test_support/test_types.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::runtime::internal::test_support::FullLiteTestStructure, YRTTestSupportFullLiteTestStructure, void> {
    static ::yandex::maps::runtime::internal::test_support::FullLiteTestStructure from(
        YRTTestSupportFullLiteTestStructure* platformFullLiteTestStructure);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::runtime::internal::test_support::FullLiteTestStructure, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YRTTestSupportFullLiteTestStructure*>::value>::type> {
    static ::yandex::maps::runtime::internal::test_support::FullLiteTestStructure from(
        PlatformType platformFullLiteTestStructure)
    {
        return ToNative<::yandex::maps::runtime::internal::test_support::FullLiteTestStructure, YRTTestSupportFullLiteTestStructure>::from(
            platformFullLiteTestStructure);
    }
};

template <>
struct ToPlatform<::yandex::maps::runtime::internal::test_support::FullLiteTestStructure> {
    static YRTTestSupportFullLiteTestStructure* from(
        const ::yandex::maps::runtime::internal::test_support::FullLiteTestStructure& fullLiteTestStructure);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
