#import <Foundation/Foundation.h>

/// @cond EXCLUDE
/**
 * The status of the current connection to the internet.
 */
typedef NS_ENUM(NSUInteger, YRTConnectivityStatus) {

    /**
     * No connection to the internet.
     */
    YRTConnectivityStatusNone,

    /**
     * Mobile network connection.
     */
    YRTConnectivityStatusCellular,

    /**
     * Wi-Fi network connection.
     */
    YRTConnectivityStatusWifi,

    /**
     * Network connection via ethernet cable.
     */
    YRTConnectivityStatusEthernet,

    /**
     * Network connection over Bluetooth.
     */
    YRTConnectivityStatusBluetooth
};
/// @endcond

