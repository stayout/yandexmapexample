#import <Foundation/Foundation.h>

@interface YRTCanonicalUnit : NSObject

@property (nonatomic, readonly, nonnull) NSString *unit;

@property (nonatomic, readonly) double value;


+ (nonnull YRTCanonicalUnit *)canonicalUnitWithUnit:(nonnull NSString *)unit
                                              value:( double)value;


@end

