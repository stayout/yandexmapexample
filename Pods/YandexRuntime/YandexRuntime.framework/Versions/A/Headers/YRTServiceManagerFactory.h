#import <YandexRuntime/YRTPlatformBinding.h>

@class YRTServiceManager;

@interface YRTServiceManagerFactory : YRTPlatformBinding

/// @cond EXCLUDE
+ (nullable YRTServiceManager *)getServiceManager;
/// @endcond


@end
