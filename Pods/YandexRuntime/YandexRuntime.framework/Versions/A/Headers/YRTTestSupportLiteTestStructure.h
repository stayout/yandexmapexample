#import <Foundation/Foundation.h>

/// @cond EXCLUDE
@interface YRTTestSupportLiteTestStructure : NSObject

@property (nonatomic, readonly) BOOL b;

@property (nonatomic, readonly, nonnull) NSString *text;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *optionalText;

@property (nonatomic, readonly) NSTimeInterval interval;

@property (nonatomic, readonly, nonnull) NSDate *timestamp;


+ (nonnull YRTTestSupportLiteTestStructure *)liteTestStructureWithB:( BOOL)b
                                                               text:(nonnull NSString *)text
                                                       optionalText:(nullable NSString *)optionalText
                                                           interval:( NSTimeInterval)interval
                                                          timestamp:(nonnull NSDate *)timestamp;


@end
/// @endcond

