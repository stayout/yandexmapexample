#import <Foundation/Foundation.h>

@protocol YMKResourceUrlProvider <NSObject>

- (nonnull NSString *)formatUrlWithResourceId:(nonnull NSString *)resourceId;


@end
