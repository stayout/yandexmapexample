#import <YandexMapKit/YMKMasstransitLineAtStop.h>
#import <YandexMapKit/YMKMasstransitStop.h>

/// @cond EXCLUDE
/**
 * @brief Contains information about a mass transit stop and the mass
 * transit lines that go through it.
 */
@interface YMKMasstransitStopMetadata : NSObject

/**
 * Mass transit stop.
 */
@property (nonatomic, readonly, nonnull) YMKMasstransitStop *stop;

/**
 * Collection of mass transit lines that go through the specified stop.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKMasstransitLineAtStop *> *linesAtStop;


+ (nonnull YMKMasstransitStopMetadata *)stopMetadataWithStop:(nonnull YMKMasstransitStop *)stop
                                                 linesAtStop:(nonnull NSArray<YMKMasstransitLineAtStop *> *)linesAtStop;


@end
/// @endcond

