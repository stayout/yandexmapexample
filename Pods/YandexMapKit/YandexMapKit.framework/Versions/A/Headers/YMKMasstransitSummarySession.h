#import <YandexMapKit/YMKMasstransitRouteMetadata.h>

#import <YandexRuntime/YRTPlatformBinding.h>

typedef void(^YMKMasstransitSummarySessionSummaryHandler)(
    NSArray<YMKMasstransitRouteMetadata *> *routes,
    NSError *error);

/**
 * Handler for an async request for a summary of mass transit routes.
 */
@interface YMKMasstransitSummarySession : YRTPlatformBinding

/**
 * Tries to cancel the current request for a summary of mass transit
 * routes.
 */
- (void)cancel;


/**
 * Retries the request for a summary of mass transit routes, using the
 * specified callback.
 */
- (void)retryWithSummaryHandler:(nullable YMKMasstransitSummarySessionSummaryHandler)summaryHandler;


@end

