#import <YandexMapKit/YMKPhotosImageSession.h>

#import <YandexRuntime/YRTPlatformBinding.h>

@class YMKPhotosSession;

/// @cond EXCLUDE
/**
 * Presents photos requesting an API.
 */
@interface YMKPhotosManager : YRTPlatformBinding

/**
 * Returns PhotoSession, which represents a photo feed of the given
 * business.
 */
- (nullable YMKPhotosSession *)photosWithBusinessId:(nonnull NSString *)businessId;


/**
 * Returns PhotoSession, which represents a photo feed of the given
 * business. The feed only contains photos with the given tags.
 */
- (nullable YMKPhotosSession *)photosWithBusinessId:(nonnull NSString *)businessId
                                               tags:(nonnull NSArray<NSString *> *)tags;


/**
 * Requests the image with a particular ID and size.
 */
- (nullable YMKPhotosImageSession *)imageWithId:(nonnull NSString *)id
                                           size:(nonnull NSString *)size
                                        handler:(nullable YMKPhotosImageSessionHandler)handler;


/**
 * Removes all cached images.
 */
- (void)clear;


@end
/// @endcond

