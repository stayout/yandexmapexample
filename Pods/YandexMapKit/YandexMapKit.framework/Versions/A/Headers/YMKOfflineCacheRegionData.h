#import <YandexMapKit/YMKLocalizedValue.h>
#import <YandexMapKit/YMKOfflineCacheRegionFile.h>
#import <YandexMapKit/YMKOfflineCacheRegionState.h>
#import <YandexMapKit/YMKPoint.h>

/**
 * @attention This feature is not available in the free MapKit version.
 */
@interface YMKOfflineCacheRegionData : NSObject

@property (nonatomic, readonly) NSUInteger id;

@property (nonatomic, readonly, nonnull) NSString *name;

@property (nonatomic, readonly, nonnull) NSString *country;

@property (nonatomic, readonly, nonnull) NSArray<NSString *> *cities;

@property (nonatomic, readonly, nonnull) YMKPoint *center;

@property (nonatomic, readonly, nonnull) YMKLocalizedValue *size;

@property (nonatomic, readonly, nonnull) NSArray<YMKOfflineCacheRegionFile *> *files;

@property (nonatomic, readonly) long long releaseTime;

@property (nonatomic, readonly) YMKOfflineCacheRegionState state;

@property (nonatomic, readonly) BOOL outdated;


+ (nonnull YMKOfflineCacheRegionData *)regionDataWithId:( NSUInteger)id
                                                   name:(nonnull NSString *)name
                                                country:(nonnull NSString *)country
                                                 cities:(nonnull NSArray<NSString *> *)cities
                                                 center:(nonnull YMKPoint *)center
                                                   size:(nonnull YMKLocalizedValue *)size
                                                  files:(nonnull NSArray<YMKOfflineCacheRegionFile *> *)files
                                            releaseTime:( long long)releaseTime
                                                  state:( YMKOfflineCacheRegionState)state
                                               outdated:( BOOL)outdated;


@end

