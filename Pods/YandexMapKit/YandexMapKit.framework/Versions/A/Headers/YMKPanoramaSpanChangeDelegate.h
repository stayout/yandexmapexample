#import <Foundation/Foundation.h>

@class YMKPanoramaPlayer;

@protocol YMKPanoramaSpanChangeDelegate <NSObject>

/**
 * Called if the user changed the zoom level or the span has been
 * changed by the setSpan method.
 *
 * @param player Panorama player that sent the event.
 */
- (void)onPanoramaSpanChangedWithPlayer:(nullable YMKPanoramaPlayer *)player;


@end
