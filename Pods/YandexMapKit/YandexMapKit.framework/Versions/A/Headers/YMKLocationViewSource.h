#import <YandexRuntime/YRTPlatformBinding.h>

/**
 * Base class interface of a location-view source.
 */
@interface YMKLocationViewSource : YRTPlatformBinding

@end

