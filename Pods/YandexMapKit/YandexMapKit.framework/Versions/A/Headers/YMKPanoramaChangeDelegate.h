#import <Foundation/Foundation.h>

@class YMKPanoramaPlayer;

@protocol YMKPanoramaChangeDelegate <NSObject>

/**
 * Called if the panorama was opened or changed by the user. You can get
 * the panoramaId by using the panoramaId() method.
 *
 * @param player Panorama player that sent the event.
 */
- (void)onPanoramaChangedWithPlayer:(nullable YMKPanoramaPlayer *)player;


@end
