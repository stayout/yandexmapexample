#import <YandexRuntime/YRTPlatformBinding.h>

#import <UIKit/UIKit.h>

/// @cond EXCLUDE
typedef void(^YMKBitmapSessionBitmapListener)(
    UIImage *bitmap,
    NSError *error);

/**
 * Session for receiving search-related bitmaps (e.g. advertisement
 * images).
 *
 * - Should be stored until listener is notified. - Can be used to
 * cancel active request. - Can be used to retry last request (usually
 * if it failed).
 */
@interface YMKBitmapSession : YRTPlatformBinding

/**
 * Cancel current request.
 */
- (void)cancel;


/**
 * Retry last request. If there is an active request, it is cancelled.
 *
 * @param bitmapListener new listener to be notified
 */
- (void)retryWithBitmapListener:(nullable YMKBitmapSessionBitmapListener)bitmapListener;


@end
/// @endcond

