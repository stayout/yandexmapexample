#import <YandexMapKit/YMKMasstransitRawTransfer.h>
#import <YandexMapKit/YMKMasstransitRawWalk.h>
#import <YandexMapKit/YMKMasstransitTransport.h>
#import <YandexMapKit/YMKMasstransitTravelEstimation.h>
#import <YandexMapKit/YMKMasstransitWait.h>
#import <YandexMapKit/YMKMasstransitWeight.h>

@interface YMKMasstransitRawSectionMetadata : NSObject

@property (nonatomic, readonly, nonnull) YMKMasstransitWeight *weight;

/**
 * choice
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKMasstransitWait *wait;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKMasstransitRawWalk *walk;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKMasstransitRawTransfer *transfer;

@property (nonatomic, readonly, nonnull) NSArray<YMKMasstransitTransport *> *transports;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKMasstransitTravelEstimation *estimation;


+ (nonnull YMKMasstransitRawSectionMetadata *)rawSectionMetadataWithWeight:(nonnull YMKMasstransitWeight *)weight
                                                                      wait:(nullable YMKMasstransitWait *)wait
                                                                      walk:(nullable YMKMasstransitRawWalk *)walk
                                                                  transfer:(nullable YMKMasstransitRawTransfer *)transfer
                                                                transports:(nonnull NSArray<YMKMasstransitTransport *> *)transports
                                                                estimation:(nullable YMKMasstransitTravelEstimation *)estimation;


@end

