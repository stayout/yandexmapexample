#import <YandexMapKit/YMKMasstransitThread.h>

@interface YMKMasstransitRawThreadMetadata : NSObject

@property (nonatomic, readonly, nonnull) YMKMasstransitThread *thread;


+ (nonnull YMKMasstransitRawThreadMetadata *)rawThreadMetadataWithThread:(nonnull YMKMasstransitThread *)thread;


@end

