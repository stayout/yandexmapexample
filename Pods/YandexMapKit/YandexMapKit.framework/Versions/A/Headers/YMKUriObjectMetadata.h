#import <YandexMapKit/YMKUri.h>

@interface YMKUriObjectMetadata : NSObject

@property (nonatomic, readonly, nonnull) NSArray<YMKUri *> *uris;


+ (nonnull YMKUriObjectMetadata *)uriObjectMetadataWithUris:(nonnull NSArray<YMKUri *> *)uris;


@end

