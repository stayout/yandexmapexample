#import <YandexMapKit/YMKDrivingJamSegment.h>

#import <UIKit/UIKit.h>

/**
 * Color for specific level of traffic intensity.
 */
@interface YMKJamTypeColor : NSObject

@property (nonatomic, readonly) YMKDrivingJamType jamType;

@property (nonatomic, readonly, nonnull) UIColor *jamColor;


+ (nonnull YMKJamTypeColor *)jamTypeColorWithJamType:( YMKDrivingJamType)jamType
                                            jamColor:(nonnull UIColor *)jamColor;


@end

