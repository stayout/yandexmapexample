#import <YandexMapKit/YMKSearchFacet.h>

/// @cond EXCLUDE
/**
 * Snippet for company ratings.
 */
@interface YMKSearchBusinessRatingObjectMetadata : NSObject

/**
 * Total number of ratings.
 */
@property (nonatomic, readonly) NSUInteger ratings;

/**
 * Total number of reviews.
 */
@property (nonatomic, readonly) NSUInteger reviews;

/**
 * Average rating score for the company.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *score;

/**
 * List of separate rating facets.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKSearchFacet *> *facets;


+ (nonnull YMKSearchBusinessRatingObjectMetadata *)businessRatingObjectMetadataWithRatings:( NSUInteger)ratings
                                                                                   reviews:( NSUInteger)reviews
                                                                                     score:(nullable NSNumber *)score
                                                                                    facets:(nonnull NSArray<YMKSearchFacet *> *)facets;


@end
/// @endcond

