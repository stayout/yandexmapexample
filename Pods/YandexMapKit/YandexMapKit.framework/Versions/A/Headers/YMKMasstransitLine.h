#import <Foundation/Foundation.h>

@class YMKMasstransitLineStyle;

/**
 * Describes a public transport line.
 */
@interface YMKMasstransitLine : NSObject

/**
 * Line ID.
 */
@property (nonatomic, readonly, nonnull) NSString *id;

/**
 * Line name.
 */
@property (nonatomic, readonly, nonnull) NSString *name;

/**
 * List of line types. Starts from the most detailed, ends with the most
 * general.
 */
@property (nonatomic, readonly, nonnull) NSArray<NSString *> *vehicleTypes;

/**
 * Line style; see YMKMasstransitLineStyle.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKMasstransitLineStyle *style;

/**
 * True if the line operates only at night.
 */
@property (nonatomic, readonly) BOOL isNight;


+ (nonnull YMKMasstransitLine *)lineWithId:(nonnull NSString *)id
                                      name:(nonnull NSString *)name
                              vehicleTypes:(nonnull NSArray<NSString *> *)vehicleTypes
                                     style:(nullable YMKMasstransitLineStyle *)style
                                   isNight:( BOOL)isNight;


@end


/**
 * Describes the style of line.
 */
@interface YMKMasstransitLineStyle : NSObject

/**
 * Line color in #RRGGBB format.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *color;


+ (nonnull YMKMasstransitLineStyle *)styleWithColor:(nullable NSNumber *)color;


@end

