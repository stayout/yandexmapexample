#import <YandexMapKit/YMKMasstransitRouteMetadata.h>
#import <YandexMapKit/YMKMasstransitSection.h>
#import <YandexMapKit/YMKPolyline.h>
#import <YandexMapKit/YMKUriObjectMetadata.h>

#import <YandexRuntime/YRTPlatformBinding.h>

@class YMKMasstransitRouter;

/**
 * Contains information about a route constructed by the mass transit
 * router.
 */
@interface YMKMasstransitRoute : YRTPlatformBinding

/**
 * General route information.
 */
@property (nonatomic, readonly, nonnull) YMKMasstransitRouteMetadata *metadata;

/**
 * Vector of sections of the route.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKMasstransitSection *> *sections;

/**
 * Polyline of a whole route.
 */
@property (nonatomic, readonly, nonnull) YMKPolyline *geometry;

/**
 * @brief Route URI, which can be used together with
 * YMKMasstransitRouter to fetch additional information about the route
 * or can be bookmarked for future reference.
 */
@property (nonatomic, readonly, nonnull) YMKUriObjectMetadata *uriMetadata;

@end

