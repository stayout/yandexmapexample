#import <YandexMapKit/YMKMasstransitRouteSettings.h>
#import <YandexMapKit/YMKMasstransitTravelEstimation.h>
#import <YandexMapKit/YMKMasstransitWeight.h>

/**
 * Contains information associated with a route constructed by the mass
 * transit router.
 */
@interface YMKMasstransitRouteMetadata : NSObject

/**
 * Contains the route time, distance of the walking part, and the number
 * of transfers.
 */
@property (nonatomic, readonly, nonnull) YMKMasstransitWeight *weight;

/**
 * Route settings that were used by the mass transit router.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKMasstransitRouteSettings *settings;

/**
 * Arrival and departure time estimations for time-dependent routes.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKMasstransitTravelEstimation *estimation;


+ (nonnull YMKMasstransitRouteMetadata *)routeMetadataWithWeight:(nonnull YMKMasstransitWeight *)weight
                                                        settings:(nullable YMKMasstransitRouteSettings *)settings
                                                      estimation:(nullable YMKMasstransitTravelEstimation *)estimation;


@end

