#import <Foundation/Foundation.h>

@class YMKGuidanceLocalizedPhrase;

/// @cond EXCLUDE
@protocol YMKGuidanceLocalizedSpeaker <NSObject>

/**
 * Stops all speech and forgets all previously scheduled phrases.
 */
- (void)reset;


/**
 * Pronounces the phrase, interrupting the one being spoken now, if
 * neccessary.
 */
- (void)sayWithPhrase:(nullable YMKGuidanceLocalizedPhrase *)phrase;


/**
 * Returns the phrase duration (how many seconds it takes to pronounce
 * it).
 */
- (double)durationWithPhrase:(nullable YMKGuidanceLocalizedPhrase *)phrase;


@end
/// @endcond
