#import <Foundation/Foundation.h>

/**
 * Type of a point on a pedestrian path.
 */
typedef NS_ENUM(NSUInteger, YMKPedestrianSpotType) {

    YMKPedestrianSpotTypeUnknown,

    YMKPedestrianSpotTypeGates
};


/**
 * Defines a point on a pedestrian path.
 */
@interface YMKSpot : NSObject

/**
 * Type of point.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *type;

/**
 * Index of the point in the section geometry.
 */
@property (nonatomic, readonly) NSUInteger position;


+ (nonnull YMKSpot *)spotWithType:(nullable NSNumber *)type
                         position:( NSUInteger)position;


@end

