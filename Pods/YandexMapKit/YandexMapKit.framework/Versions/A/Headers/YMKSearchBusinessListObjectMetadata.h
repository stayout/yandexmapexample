#import <Foundation/Foundation.h>

@class YMKSearchBusinessListObjectMetadataBusiness;

/**
 * Snippet for collection of companies. Used for listing companies
 * associated with the toponym (usually house).
 */
@interface YMKSearchBusinessListObjectMetadata : NSObject

/**
 * Company list.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKSearchBusinessListObjectMetadataBusiness *> *businessList;


+ (nonnull YMKSearchBusinessListObjectMetadata *)businessListObjectMetadataWithBusinessList:(nonnull NSArray<YMKSearchBusinessListObjectMetadataBusiness *> *)businessList;


@end


/**
 * Company description.
 */
@interface YMKSearchBusinessListObjectMetadataBusiness : NSObject

/**
 * Identifier to get extended information about company.
 */
@property (nonatomic, readonly, nonnull) NSString *id;

/**
 * Human readable company name.
 */
@property (nonatomic, readonly, nonnull) NSString *name;


+ (nonnull YMKSearchBusinessListObjectMetadataBusiness *)businessWithId:(nonnull NSString *)id
                                                                   name:(nonnull NSString *)name;


@end

