#import <YandexMapKit/YMKSearchAvailability.h>

/**
 * Open hours for an organization.
 */
@interface YMKSearchWorkingHours : NSObject

/**
 * Human-readable localized open hours description. For example,
 * "пн-пт 10:00-20:00".
 */
@property (nonatomic, readonly, nonnull) NSString *text;

/**
 * Structured open hours information.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKSearchAvailability *> *availabilities;


+ (nonnull YMKSearchWorkingHours *)workingHoursWithText:(nonnull NSString *)text
                                         availabilities:(nonnull NSArray<YMKSearchAvailability *> *)availabilities;


@end

