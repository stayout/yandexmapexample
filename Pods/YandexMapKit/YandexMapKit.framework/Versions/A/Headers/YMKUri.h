#import <Foundation/Foundation.h>

@interface YMKUri : NSObject

@property (nonatomic, readonly, nonnull) NSString *value;


+ (nonnull YMKUri *)uriWithValue:(nonnull NSString *)value;


@end

