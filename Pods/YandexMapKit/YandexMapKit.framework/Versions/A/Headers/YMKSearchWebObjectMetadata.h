#import <YandexMapKit/YMKSpannableString.h>

/**
 * Additional data for web objects.
 */
@interface YMKSearchWebObjectMetadata : NSObject

/**
 * Web page title.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKSpannableString *highlightedTitle;

/**
 * Web page url.
 */
@property (nonatomic, readonly, nonnull) NSString *url;

/**
 * Relevant parts of web page.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKSpannableString *> *highlightedSnippets;


+ (nonnull YMKSearchWebObjectMetadata *)webObjectMetadataWithHighlightedTitle:(nullable YMKSpannableString *)highlightedTitle
                                                                          url:(nonnull NSString *)url
                                                          highlightedSnippets:(nonnull NSArray<YMKSpannableString *> *)highlightedSnippets;


@end

