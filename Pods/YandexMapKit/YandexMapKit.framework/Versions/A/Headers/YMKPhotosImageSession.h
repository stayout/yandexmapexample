#import <YandexRuntime/YRTPlatformBinding.h>

#import <UIKit/UIKit.h>

/// @cond EXCLUDE
typedef void(^YMKPhotosImageSessionHandler)(
    UIImage *bitmap,
    NSError *error);

/**
 * Provides images.
 */
@interface YMKPhotosImageSession : YRTPlatformBinding

/**
 * Cancels the pending image request, if there is one.
 */
- (void)cancel;


/**
 * Re-fetches the image with the same parameters, but a different
 * listener.
 */
- (void)retryWithHandler:(nullable YMKPhotosImageSessionHandler)handler;


@end
/// @endcond

