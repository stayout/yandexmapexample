#import <Foundation/Foundation.h>

/**
 * Represents a 'wait until suitable tranport arrives' section of a
 * route.
 */
@interface YMKMasstransitWait : NSObject

@property (nonatomic, readonly) NSUInteger dummy;


+ (nonnull YMKMasstransitWait *)waitWithDummy:( NSUInteger)dummy;


@end

