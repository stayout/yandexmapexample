#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, YMKUserLocationIconType) {

    YMKUserLocationIconTypeArrow,

    YMKUserLocationIconTypePin
};


typedef NS_ENUM(NSUInteger, YMKUserLocationAnchorType) {

    YMKUserLocationAnchorTypeNormal,

    YMKUserLocationAnchorTypeCourse
};

