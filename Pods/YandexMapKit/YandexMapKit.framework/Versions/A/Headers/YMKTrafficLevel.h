#import <YandexMapKit/YMKTrafficListener.h>

@interface YMKTrafficLevel : NSObject

@property (nonatomic, readonly) YMKTrafficColor color;

@property (nonatomic, readonly) NSInteger level;


+ (nonnull YMKTrafficLevel *)trafficLevelWithColor:( YMKTrafficColor)color
                                             level:( NSInteger)level;


@end

