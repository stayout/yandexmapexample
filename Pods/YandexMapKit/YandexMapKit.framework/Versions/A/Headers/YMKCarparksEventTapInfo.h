#import <Foundation/Foundation.h>

/// @cond EXCLUDE
@interface YMKCarparksEventTapInfo : NSObject

/**
 * Event identifier.
 */
@property (nonatomic, readonly, nonnull) NSString *id;

/**
 * Event age in seconds.
 */
@property (nonatomic, readonly) long long age;


+ (nonnull YMKCarparksEventTapInfo *)carparksEventTapInfoWithId:(nonnull NSString *)id
                                                            age:( long long)age;


@end
/// @endcond

