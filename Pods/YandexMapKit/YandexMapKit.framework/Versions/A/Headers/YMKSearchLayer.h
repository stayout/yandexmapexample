#import <YandexMapKit/YMKAssetsProvider.h>
#import <YandexMapKit/YMKGeometry.h>
#import <YandexMapKit/YMKPlacemarkListener.h>
#import <YandexMapKit/YMKResponseHandler.h>
#import <YandexMapKit/YMKSearchBusinessFilter.h>
#import <YandexMapKit/YMKSearchManager.h>
#import <YandexMapKit/YMKSearchMetadata.h>
#import <YandexMapKit/YMKSearchOptions.h>

#import <YandexRuntime/YRTPlatformBinding.h>

@class YMKSearchResultItem;

typedef NS_ENUM(NSUInteger, YMKGeometrySearchType) {

    /**
     * Search along the whole geometry.
     */
    YMKGeometrySearchTypeWhole,

    /**
     * Search along the geometry in the current window.
     */
    YMKGeometrySearchTypeCurrentWindow
};


@interface YMKSearchLayer : YRTPlatformBinding

- (void)submitQueryWithQuery:(nonnull NSString *)query
               searchOptions:(nonnull YMKSearchOptions *)searchOptions;


- (void)resubmit;


- (BOOL)hasNextPage;


- (void)fetchNextPage;


- (void)clear;


- (nonnull NSArray<YMKSearchResultItem *> *)getVisibleResults;


- (nonnull YMKSearchMetadata *)searchMetadata;


/**
 * Do not use this method; it is for internal use only.
 */
- (void)setSearchManagerWithSearchManager:(nullable YMKSearchManager *)searchManager;


- (void)addSearchResultListenerWithSearchResultListener:(nullable id<YMKResponseHandler>)searchResultListener;


- (void)removeSearchResultListenerWithSearchResultListener:(nullable id<YMKResponseHandler>)searchResultListener;


- (void)addPlacemarkListenerWithSearchResultListener:(nullable id<YMKPlacemarkListener>)searchResultListener;


- (void)removePlacemarkListenerWithSearchResultListener:(nullable id<YMKPlacemarkListener>)searchResultListener;


- (void)setSortByRank;


- (void)setSortByDistanceWithOrigin:(nonnull YMKGeometry *)origin
                 geometrySearchType:(YMKGeometrySearchType)geometrySearchType;


- (void)setFiltersWithFilters:(nonnull NSArray<YMKSearchBusinessFilter *> *)filters;


- (void)setAssetsProviderWithProvider:(nullable id<YMKAssetsProvider>)provider;


- (void)selectPlacemarkWithGeoObjectId:(nonnull NSString *)geoObjectId;


- (void)deselectPlacemark;


/**
 * Tells if this object is valid or no. Any method called on an invalid
 * object will throw an exception. The object becomes invalid only on UI
 * thread, and only when its implementation depends on objects already
 * destroyed by now. Please refer to general docs about the interface for
 * details on its invalidation.
 */
@property (nonatomic, readonly, getter=isValid) BOOL valid;

@end

