#import <YandexMapKit/YMKMasstransitSession.h>
#import <YandexMapKit/YMKPoint.h>
#import <YandexMapKit/YMKTimeOptions.h>

#import <YandexRuntime/YRTPlatformBinding.h>

/**
 * Provides methods for submitting pedestrian routing requests.
 */
@interface YMKPedestrianRouter : YRTPlatformBinding

/**
 * Submits a request to find a pedestrian route between two points.
 * Currently, at most one route is returned (no alternatives are
 * supported).
 *
 * @param source Origin point of the route to find.
 * @param destination Destination point of the route to find.
 * @param timeOptions Desired departure/arrival time settings. Empty
 * YMKTimeOptions for requests that are not time-dependent.
 * @param routeListener Listener to retrieve a list of MasstransitRoute
 * objects.
 */
- (nullable YMKMasstransitSession *)requestRoutesWithSource:(nonnull YMKPoint *)source
                                                destination:(nonnull YMKPoint *)destination
                                                timeOptions:(nonnull YMKTimeOptions *)timeOptions
                                               routeHandler:(nullable YMKMasstransitSessionRouteHandler)routeHandler;


/**
 * Submits a request to retrieve detailed information on the pedestrian
 * route by URI.
 *
 * @param uri Pedestrian route URI. Begins with
 * "ymapsbm1://route/pedestrian".
 * @param timeOptions Desired departure/arrival time settings. Empty
 * YMKTimeOptions for requests that are not time-dependent.
 * @param routeListener Listener to retrieve a list of MasstransitRoute
 * objects.
 */
- (nullable YMKMasstransitSession *)resolveUriWithUri:(nonnull NSString *)uri
                                          timeOptions:(nonnull YMKTimeOptions *)timeOptions
                                         routeHandler:(nullable YMKMasstransitSessionRouteHandler)routeHandler;


@end

