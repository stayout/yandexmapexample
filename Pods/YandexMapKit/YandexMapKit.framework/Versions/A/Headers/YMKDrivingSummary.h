#import <YandexMapKit/YMKDrivingFlags.h>
#import <YandexMapKit/YMKDrivingWeight.h>

@interface YMKDrivingSummary : NSObject

@property (nonatomic, readonly, nonnull) YMKDrivingWeight *weight;

@property (nonatomic, readonly, nonnull) YMKDrivingFlags *flags;


+ (nonnull YMKDrivingSummary *)summaryWithWeight:(nonnull YMKDrivingWeight *)weight
                                           flags:(nonnull YMKDrivingFlags *)flags;


@end

