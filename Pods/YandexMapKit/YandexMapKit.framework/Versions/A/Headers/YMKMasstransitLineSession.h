#import <YandexMapKit/YMKMasstransitLine.h>
#import <YandexMapKit/YMKMasstransitLineInfo.h>

#import <YandexRuntime/YRTPlatformBinding.h>

/// @cond EXCLUDE
typedef void(^YMKMasstransitLineSessionLineHandler)(
    YMKMasstransitLineInfo *lineInfo,
    NSError *error);

/**
 * Handler for a mass transit YMKMasstransitLine async request.
 */
@interface YMKMasstransitLineSession : YRTPlatformBinding

/**
 * Tries to cancel the current mass transit line request.
 */
- (void)cancel;


/**
 * Retries the mass transit line request using the specified callback.
 */
- (void)retryWithLineHandler:(nullable YMKMasstransitLineSessionLineHandler)lineHandler;


@end
/// @endcond

