#import <YandexMapKit/YMKSpeedLimits.h>
#import <YandexMapKit/YMKSpeedLimitsRules.h>

#import <YandexRuntime/YRTPlatformBinding.h>

/// @cond EXCLUDE
@interface YMKSpeedingPolicy : YRTPlatformBinding

/**
 * E.g. for Russia 60/90/110 km/h
 */
@property (nonatomic, readonly, nonnull) YMKSpeedLimits *legalSpeedLimits;

/**
 * E.g. ratio = 0.8 and current speed limit is 60 km/h - for Russia:
 * enforcement tolerance is 20 km/h for all speed limits => we should
 * start warning the user when current speed > 60 + 20 * 0.8 = 76 km/h -
 * for Turkey: enforcement tolerance is 10 percent over the speed limit
 * => we should start warning the user when current speed > 60 + 60 *
 * 0.1 * 0.8 = 64.8 km/h
 */
- (nonnull YMKSpeedLimits *)customSpeedLimitsWithToleranceRatio:(double)toleranceRatio;


@property (nonatomic, readonly, nonnull) YMKSpeedLimitsRules *speedLimitsRules;

/**
 * Optional property, can be nil.
 */
@property (nonatomic, readonly, nullable) NSNumber *region;

/**
 * Tells if this object is valid or no. Any method called on an invalid
 * object will throw an exception. The object becomes invalid only on UI
 * thread, and only when its implementation depends on objects already
 * destroyed by now. Please refer to general docs about the interface for
 * details on its invalidation.
 */
@property (nonatomic, readonly, getter=isValid) BOOL valid;

@end
/// @endcond

