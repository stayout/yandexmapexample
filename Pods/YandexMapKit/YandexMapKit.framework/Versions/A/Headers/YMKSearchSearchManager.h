#import <Foundation/Foundation.h>

/**
 * Search manager type. Describes difference in online/offline search
 * handling.
 */
typedef NS_ENUM(NSUInteger, YMKSearchSearchManagerType) {

    /**
     * @attention This feature is not available in the free MapKit version.
     *
     *
     * Default search manager. Uses connectivity info from phone OS to
     * determine if it should use online or offline search. Will not combine
     * results from online and offline searches in a single session. If
     * search is started in one of the modes all resubmits will be issued in
     * this mode.
     */
    YMKSearchSearchManagerTypeDefault,

    /**
     * Online search manager. Will always tries to use online search even if
     * network is not available.
     */
    YMKSearchSearchManagerTypeOnline,

    /**
     * @attention This feature is not available in the free MapKit version.
     *
     *
     * Offline search manager. Will always tries to use offline search even
     * if network is available.
     */
    YMKSearchSearchManagerTypeOffline,

    /**
     * @attention This feature is not available in the free MapKit version.
     *
     *
     * Combined search manager. Decision to use online or offline search is
     * based on internal timeout. If server is manages to respond in given
     * time, then online search result is returned. Otherwise uses offline
     * search. Will combine online and offline search result in single
     * session (hence the name). Timeout logic is applied on each resubmit.
     */
    YMKSearchSearchManagerTypeCombined
};

