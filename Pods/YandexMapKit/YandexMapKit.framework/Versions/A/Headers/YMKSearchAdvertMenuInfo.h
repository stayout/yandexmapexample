#import <YandexMapKit/YMKSearchAdvertMenuItem.h>

/// @cond EXCLUDE
/**
 * Multiple advert menu entries.
 */
@interface YMKSearchAdvertMenuInfo : NSObject

/**
 * Menu items.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKSearchAdvertMenuItem *> *menuItems;


+ (nonnull YMKSearchAdvertMenuInfo *)advertMenuInfoWithMenuItems:(nonnull NSArray<YMKSearchAdvertMenuItem *> *)menuItems;


@end
/// @endcond

