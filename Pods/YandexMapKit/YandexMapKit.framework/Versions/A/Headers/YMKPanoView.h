#import <YandexMapKit/YMKPanoramaPlayer.h>
#import <UIKit/UIKit.h>

@interface YMKPanoView : UIView

@property (strong, nonatomic, readonly) YMKPanoramaPlayer *player;

- (id)initWithFrame:(CGRect)frame;
- (void)setNoninteractive:(bool)is;

@end
