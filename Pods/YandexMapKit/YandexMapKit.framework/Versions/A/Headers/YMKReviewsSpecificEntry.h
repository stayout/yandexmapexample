#import <YandexMapKit/YMKReviewsReviews.h>
#import <YandexMapKit/YMKReviewsReviewsTag.h>

/// @cond EXCLUDE
/**
 * Review data.
 */
@interface YMKReviewsSpecificEntry : NSObject

/**
 * Business ID.
 */
@property (nonatomic, readonly, nonnull) NSString *businessId;

/**
 * Review text.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *descriptionText;

/**
 * Snippet of review text (being set on server).
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *snippet;

/**
 * Average rating.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *rating;

/**
 * Positive votes.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *positive;

/**
 * Negative votes.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *negative;

/**
 * Vote of review owner.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *userVote;

/**
 * Evaluation by facets.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKReviewsReviewsTag *> *tag;

/**
 * Review processing status.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *status;


+ (nonnull YMKReviewsSpecificEntry *)specificEntryWithBusinessId:(nonnull NSString *)businessId
                                                 descriptionText:(nullable NSString *)descriptionText
                                                         snippet:(nullable NSString *)snippet
                                                          rating:(nullable NSNumber *)rating
                                                        positive:(nullable NSNumber *)positive
                                                        negative:(nullable NSNumber *)negative
                                                        userVote:(nullable NSNumber *)userVote
                                                             tag:(nonnull NSArray<YMKReviewsReviewsTag *> *)tag
                                                          status:(nullable NSNumber *)status;


@end
/// @endcond

