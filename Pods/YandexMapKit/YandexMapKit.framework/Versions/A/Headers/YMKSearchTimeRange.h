#import <Foundation/Foundation.h>

/**
 * The time interval used to compose availability info.
 *
 * Can describe two kinds of intervals: 1. 24-hour interval
 * (`isTwentyFourHours` is true, `from` and `to` are not used). 2.
 * Smaller time interval (`isTwentyFourHours` is false, `from` and `to`
 * are set to the begin and end of the interval)
 */
@interface YMKSearchTimeRange : NSObject

/**
 * All day (24 hours) time range marker.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *isTwentyFourHours;

/**
 * Interval start (seconds from midnight).
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *from;

/**
 * Interval end (seconds from midnight).
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *to;


+ (nonnull YMKSearchTimeRange *)timeRangeWithIsTwentyFourHours:(nullable NSNumber *)isTwentyFourHours
                                                          from:(nullable NSNumber *)from
                                                            to:(nullable NSNumber *)to;


@end

