#import <Foundation/Foundation.h>

/**
 * The length of the U-turn.
 */
@interface YMKDrivingUturnMetadata : NSObject

/**
 * The length оf the turn.
 */
@property (nonatomic, readonly) double length;


+ (nonnull YMKDrivingUturnMetadata *)uturnMetadataWithLength:( double)length;


@end

