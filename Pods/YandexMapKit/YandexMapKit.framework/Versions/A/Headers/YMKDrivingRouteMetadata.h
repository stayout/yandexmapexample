#import <YandexMapKit/YMKDrivingDescription.h>
#import <YandexMapKit/YMKDrivingFlags.h>
#import <YandexMapKit/YMKDrivingWeight.h>
#import <YandexMapKit/YMKPoint.h>

@interface YMKDrivingRouteMetadata : NSObject

@property (nonatomic, readonly, nonnull) YMKDrivingWeight *weight;

@property (nonatomic, readonly, nonnull) YMKDrivingFlags *flags;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSData *descriptor;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSData *traits;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKDrivingDescription *description;

@property (nonatomic, readonly, nonnull) NSArray<YMKPoint *> *selectedArrivalPoints;


+ (nonnull YMKDrivingRouteMetadata *)routeMetadataWithWeight:(nonnull YMKDrivingWeight *)weight
                                                       flags:(nonnull YMKDrivingFlags *)flags
                                                  descriptor:(nullable NSData *)descriptor
                                                      traits:(nullable NSData *)traits
                                                 description:(nullable YMKDrivingDescription *)description
                                       selectedArrivalPoints:(nonnull NSArray<YMKPoint *> *)selectedArrivalPoints;


@end

