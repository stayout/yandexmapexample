#import <YandexMapKit/YMKDrivingLeaveRoundaboutMetadata.h>
#import <YandexMapKit/YMKDrivingUturnMetadata.h>

/**
 * Information about an action.
 */
@interface YMKDrivingActionMetadata : NSObject

/**
 * The length of the U-turn.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKDrivingUturnMetadata *uturnMetadata;

/**
 * The number of the exit for leaving the roundabout.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKDrivingLeaveRoundaboutMetadata *leaveRoundaboutMetadada;


+ (nonnull YMKDrivingActionMetadata *)actionMetadataWithUturnMetadata:(nullable YMKDrivingUturnMetadata *)uturnMetadata
                                              leaveRoundaboutMetadada:(nullable YMKDrivingLeaveRoundaboutMetadata *)leaveRoundaboutMetadada;


@end

