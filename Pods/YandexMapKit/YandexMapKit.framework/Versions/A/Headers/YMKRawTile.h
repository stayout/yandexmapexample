#import <YandexMapKit/YMKVersion.h>

typedef NS_ENUM(NSUInteger, YMKRawTileState) {

    YMKRawTileStateOk,

    YMKRawTileStateNotModified,

    YMKRawTileStateError
};


@interface YMKRawTile : NSObject

@property (nonatomic, readonly, nonnull) YMKVersion *version;

@property (nonatomic, readonly, nonnull) NSString *etag;

@property (nonatomic, readonly) YMKRawTileState state;

@property (nonatomic, readonly, nonnull) NSData *rawData;


+ (nonnull YMKRawTile *)rawTileWithVersion:(nonnull YMKVersion *)version
                                      etag:(nonnull NSString *)etag
                                     state:( YMKRawTileState)state
                                   rawData:(nonnull NSData *)rawData;


@end

