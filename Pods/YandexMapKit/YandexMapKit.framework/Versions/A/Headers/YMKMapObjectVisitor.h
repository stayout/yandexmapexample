#import <Foundation/Foundation.h>

@class YMKCircleMapObject;
@class YMKColoredPolylineMapObject;
@class YMKMapObjectCollection;
@class YMKPlacemarkMapObject;
@class YMKPolygonMapObject;
@class YMKPolylineMapObject;

/**
 * Used to traverse over the children of the MapObjectCollection.
 */
@protocol YMKMapObjectVisitor <NSObject>

- (void)onPlacemarkVisitedWithPlacemark:(nullable YMKPlacemarkMapObject *)placemark;


- (void)onPolylineVisitedWithPolyline:(nullable YMKPolylineMapObject *)polyline;


- (void)onColoredPolylineVisitedWithPolyline:(nullable YMKColoredPolylineMapObject *)polyline;


- (void)onPolygonVisitedWithPolygon:(nullable YMKPolygonMapObject *)polygon;


- (void)onCircleVisitedWithCircle:(nullable YMKCircleMapObject *)circle;


/**
 * Called for every child collection. The collection is ignored if this
 * method returns false.
 */
- (BOOL)onCollectionVisitStartWithCollection:(nullable YMKMapObjectCollection *)collection;


/**
 * Called for visited collections only. If an exception occurred during
 * the visit, the method might be skipped.
 */
- (void)onCollectionVisitEndWithCollection:(nullable YMKMapObjectCollection *)collection;


@end
