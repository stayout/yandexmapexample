#import <YandexMapKit/YMKReviewsEntry.h>
#import <YandexMapKit/YMKReviewsEntrySession.h>
#import <YandexMapKit/YMKReviewsEraseSession.h>

#import <YandexRuntime/YRTPlatformBinding.h>

@class YMKReviewsSession;

/// @cond EXCLUDE
/**
 * ReviewsManager presents an API for requesting business reviews and
 * managing user reviews.
 */
@interface YMKReviewsManager : YRTPlatformBinding

/**
 * Returns ReviewSession, which represents the feed of reviews of the
 * given business.
 */
- (nullable YMKReviewsSession *)reviewsWithBusinessId:(nonnull NSString *)businessId;


/**
 * Requests a review template for the given organization. If the user
 * already has a review for this business, this review will be returned.
 */
- (nullable YMKReviewsEntrySession *)reviewTemplateWithBusinessId:(nonnull NSString *)businessId
                                                      dataHandler:(nullable YMKReviewsEntrySessionDataHandler)dataHandler;


/**
 * Adds or updates the user review for the given business.
 * 'filledTemplate.content' MUST contain 'rating' field filled.
 */
- (nullable YMKReviewsEntrySession *)updateWithFilledTemplate:(nonnull YMKReviewsEntry *)filledTemplate
                                                  dataHandler:(nullable YMKReviewsEntrySessionDataHandler)dataHandler;


/**
 * Deletes the user review for the given business.
 */
- (nullable YMKReviewsEraseSession *)eraseWithBusinessId:(nonnull NSString *)businessId
                                                 handler:(nullable YMKReviewsEraseSessionHandler)handler;


@end
/// @endcond

