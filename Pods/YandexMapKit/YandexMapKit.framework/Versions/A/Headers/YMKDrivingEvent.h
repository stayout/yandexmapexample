#import <YandexMapKit/YMKDrivingAnnotation.h>
#import <YandexMapKit/YMKPoint.h>
#import <YandexMapKit/YMKPolylinePosition.h>
#import <YandexMapKit/YMKRoadEventsRoadEvents.h>

@interface YMKDrivingEvent : NSObject

@property (nonatomic, readonly, nonnull) YMKPolylinePosition *polylinePosition;

@property (nonatomic, readonly, nonnull) NSString *eventId;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *descriptionText;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *types;

@property (nonatomic, readonly, nonnull) YMKPoint *location;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *speedLimit;

/**
 * valid only for cameras
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *annotationSchemeId;


+ (nonnull YMKDrivingEvent *)eventWithPolylinePosition:(nonnull YMKPolylinePosition *)polylinePosition
                                               eventId:(nonnull NSString *)eventId
                                       descriptionText:(nullable NSString *)descriptionText
                                                 types:(nonnull NSArray<NSNumber *> *)types
                                              location:(nonnull YMKPoint *)location
                                            speedLimit:(nullable NSNumber *)speedLimit
                                    annotationSchemeId:(nullable NSNumber *)annotationSchemeId;


@end

