#import <YandexMapKit/YMKBitmapSession.h>

@class YMKSearchBitmapDownloader;

/// @cond EXCLUDE
/**
 * Contains information about the advertisement.
 */
@interface YMKSearchAdvertisement : NSObject

/**
 * Advertisement text.
 */
@property (nonatomic, readonly, nonnull) NSString *text;

/**
 * Advertising disclaimers.
 */
@property (nonatomic, readonly, nonnull) NSArray<NSString *> *disclaimers;

/**
 * Advertisement title.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *title;

/**
 * Optional extra information.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *extra;

/**
 * Optional link.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *url;

/**
 * Optional phone for the advert.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *phone;

/**
 * External url to be visited (counter) upon displaying the
 * advertisement.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *displayCounter;

/**
 * External url to be visited (counter) upon clicking the advertisement.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *clickCounter;

/**
 * Optional style that can be used to create an icon for the
 * advertisement (see
 * YMKSearchBitmapDownloader::requestBitmapWithId:scale:bitmapListener:
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *style;

/**
 * Human-readable identifier for logging.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *logId;

/**
 * Additional string-based info.
 */
@property (nonatomic, readonly, nonnull) NSArray<NSString *> *tags;


+ (nonnull YMKSearchAdvertisement *)advertisementWithText:(nonnull NSString *)text
                                              disclaimers:(nonnull NSArray<NSString *> *)disclaimers
                                                    title:(nullable NSString *)title
                                                    extra:(nullable NSString *)extra
                                                      url:(nullable NSString *)url
                                                    phone:(nullable NSString *)phone
                                           displayCounter:(nullable NSString *)displayCounter
                                             clickCounter:(nullable NSString *)clickCounter
                                                    style:(nullable NSString *)style
                                                    logId:(nullable NSString *)logId
                                                     tags:(nonnull NSArray<NSString *> *)tags;


@end
/// @endcond

