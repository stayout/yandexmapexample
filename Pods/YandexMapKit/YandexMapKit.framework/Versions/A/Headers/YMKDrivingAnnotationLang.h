#import <Foundation/Foundation.h>

/**
 * The language of the annotations.
 */
typedef NS_ENUM(NSUInteger, YMKDrivingAnnotationLanguage) {

    YMKDrivingAnnotationLanguageRussian,

    YMKDrivingAnnotationLanguageEnglish,

    YMKDrivingAnnotationLanguageFrench,

    YMKDrivingAnnotationLanguageTurkish,

    YMKDrivingAnnotationLanguageUkrainian,

    YMKDrivingAnnotationLanguageItalian,

    YMKDrivingAnnotationLanguageAnnotationLanguage_Count
};

