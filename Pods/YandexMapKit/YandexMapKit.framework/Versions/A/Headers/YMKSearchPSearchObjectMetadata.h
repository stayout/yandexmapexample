#import <YandexMapKit/YMKLocalizedValue.h>

/**
 * Extended info for objects from user created map.
 */
@interface YMKSearchPSearchObjectMetadata : NSObject

/**
 * Object name.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *name;

/**
 * Object address.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *address;

/**
 * Object wikimap category.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *category;

/**
 * Localized distance to the object.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKLocalizedValue *distance;


+ (nonnull YMKSearchPSearchObjectMetadata *)pSearchObjectMetadataWithName:(nullable NSString *)name
                                                                  address:(nullable NSString *)address
                                                                 category:(nullable NSString *)category
                                                                 distance:(nullable YMKLocalizedValue *)distance;


@end

