#import <Foundation/Foundation.h>

@interface YMKDrivingDescription : NSObject

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *via;


+ (nonnull YMKDrivingDescription *)descriptionWithVia:(nullable NSString *)via;


@end

