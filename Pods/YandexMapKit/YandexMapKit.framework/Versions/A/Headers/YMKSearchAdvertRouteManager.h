#import <YandexMapKit/YMKGeoObject.h>
#import <YandexMapKit/YMKPolyline.h>
#import <YandexMapKit/YMKPolylinePosition.h>
#import <YandexMapKit/YMKSearchAdvertRouteListener.h>

#import <YandexRuntime/YRTPlatformBinding.h>

/// @cond EXCLUDE
/**
 * Allows to get advertisment to show along user route.
 *
 * Advertisement is selected based on route provided (this allows hiding
 * irrelevant adverts) and on provided route position (this allows
 * hiding advertisments which are left behind).
 *
 * New advertisement is received in background, notifiying user when new
 * advertisement is available.
 */
@interface YMKSearchAdvertRouteManager : YRTPlatformBinding

/**
 * Set the listener to receive route adverts. Listener may be notified
 * after calls to `setRoute` and `setRoutePosition`.
 *
 * @param routeListener listener to add
 */
- (void)addListenerWithRouteListener:(nullable id<YMKSearchAdvertRouteListener>)routeListener;


/**
 * Remove route advertisements listener.
 *
 * @param routeListener listener to remove
 */
- (void)removeListenerWithRouteListener:(nullable id<YMKSearchAdvertRouteListener>)routeListener;


/**
 * Set route geometry to receive route adverts.
 *
 * @param route route geometry. May not be null, use `resetRoute` to
 * reset.
 */
- (void)setRouteWithRoute:(nonnull YMKPolyline *)route;


/**
 * Resets route geometry. No further notifications will be issued to
 * listeneres until new route is provided.
 */
- (void)resetRoute;


/**
 * Set current position on route to receive route advert if possible.
 * Throws if no route set.
 *
 * @param point new route position value
 */
- (void)setRoutePositionWithPoint:(nonnull YMKPolylinePosition *)point;


/**
 * Current route adverts.
 *
 * @return collection of advertised `GeoObjects`
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKGeoObject *> *advertObjects;

@end
/// @endcond

