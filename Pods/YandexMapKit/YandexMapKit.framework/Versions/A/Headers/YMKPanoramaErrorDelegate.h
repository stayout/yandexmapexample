#import <Foundation/Foundation.h>

@class YMKPanoramaPlayer;

@protocol YMKPanoramaErrorDelegate <NSObject>

/**
 * Error notification listener for the panoramaOpen class. Called if the
 * panorama could not be opened.
 */
- (void)onPanoramaOpenErrorWithPlayer:(nullable YMKPanoramaPlayer *)player
                                error:(nullable NSError *)error;


@end
