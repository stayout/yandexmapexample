#import <YandexMapKit/YMKCameraPosition.h>
#import <YandexMapKit/YMKCameraUpdateSource.h>

@class YMKMap;

/**
 * Listens for updates to the camera position.
 */
@protocol YMKMapCameraListener <NSObject>

/**
 * Triggered when the camera position changed.
 */
- (void)onCameraPositionChangedWithMap:(nullable YMKMap *)map
                        cameraPosition:(nonnull YMKCameraPosition *)cameraPosition
                    cameraUpdateSource:(YMKCameraUpdateSource)cameraUpdateSource
                              finished:(BOOL)finished;


@end
