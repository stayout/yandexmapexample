#import <YandexMapKit/YMKCoverage.h>
#import <YandexMapKit/YMKDrivingRouter.h>
#import <YandexMapKit/YMKGuidanceGuide.h>
#import <YandexMapKit/YMKLocationManager.h>
#import <YandexMapKit/YMKLocationSimulator.h>
#import <YandexMapKit/YMKMapWindow.h>
#import <YandexMapKit/YMKMasstransitInfoService.h>
#import <YandexMapKit/YMKMasstransitRouter.h>
#import <YandexMapKit/YMKMyCarStateManager.h>
#import <YandexMapKit/YMKOfflineCacheManager.h>
#import <YandexMapKit/YMKPanoramaPlayer.h>
#import <YandexMapKit/YMKPanoramaService.h>
#import <YandexMapKit/YMKPedestrianRouter.h>
#import <YandexMapKit/YMKPhotosManager.h>
#import <YandexMapKit/YMKPolyline.h>
#import <YandexMapKit/YMKRecordedSimulator.h>
#import <YandexMapKit/YMKReviewsManager.h>
#import <YandexMapKit/YMKRoadEventsManager.h>
#import <YandexMapKit/YMKSearchAdvertMenuManager.h>
#import <YandexMapKit/YMKSearchAdvertRouteManager.h>
#import <YandexMapKit/YMKSearchBitmapDownloader.h>
#import <YandexMapKit/YMKSearchManager.h>
#import <YandexMapKit/YMKSearchSearchManager.h>
#import <YandexMapKit/YMKStyleType.h>
#import <YandexMapKit/YMKTaxiManager.h>
#import <YandexMapKit/YMKUiExperimentsManager.h>

#import <YandexRuntime/YRTAccount.h>
#import <YandexRuntime/YRTMiidManager.h>
#import <YandexRuntime/YRTPlatformBinding.h>
#import <YandexRuntime/YRTReportData.h>
#import <YandexRuntime/YRTReportFactory.h>

@class YRTView;

/**
 * Provides access to all services in the SDK.
 *
 * Initialize the MapKit factory before using this class.
 *
 * Note: MapKit holds listener/delegate objects by weak references. You
 * need to have strong references to them somewhere in the client code.
 */
@interface YMKMapKit : YRTPlatformBinding

/// @cond EXCLUDE
/**
 * Sets user-specific parameters for MapKit.
 *
 * @param uuid Refer to the Yandex.Metrica documentation for information
 * about what this is and how to get one.
 * @param deviceId Device ID.
 */
- (void)setMetricaIdsWithUuid:(nonnull NSString *)uuid
                     deviceId:(nonnull NSString *)deviceId;
/// @endcond


/// @cond EXCLUDE
/**
 * Sets the key for API access.
 *
 * @param key Key issued in the Developer's Dashboard.
 *
 * Remark:
 * @param key has optional type, it may be uninitialized.
 */
- (void)setApiKeyWithKey:(nullable NSString *)key;
/// @endcond


/// @cond EXCLUDE
/**
 * Sets the account that is used by services.
 */
- (void)setAccount:(nullable id<YRTAccount>)account;
/// @endcond


/// @cond EXCLUDE
/**
 * Experimental. Will be removed when Navigator switches to the MapKit
 * map.
 */
- (void)setScaleFactorWithScaleFactor:(float)scaleFactor;
/// @endcond


/// @cond EXCLUDE
- (void)setStyleTypeWithStyleType:(YMKStyleType)styleType;
/// @endcond


/**
 * Notifies MapKit when the application resumes the foreground state.
 */
- (void)onStart;


/**
 * Notifies MapKit when the application pauses and goes to the
 * background.
 */
- (void)onStop;


/// @cond EXCLUDE
/**
 * Returns the manager that uniquely identifies the installed app.
 */
@property (nonatomic, readonly, nullable) YRTMiidManager *miidManager;
/// @endcond

/// @cond EXCLUDE
/**
 * Creates a manager that allows to check if the "feature" with a given
 * ID covers some specific point or area.
 */
- (nullable YMKCoverage *)createCoverageWithCoverageId:(nonnull NSString *)coverageId;
/// @endcond


/// @cond EXCLUDE
/**
 * Returns a manager that allows to listen for experimental UI
 * parameters.
 */
@property (nonatomic, readonly, nullable) YMKUiExperimentsManager *uiExperimentsManager;
/// @endcond

/**
 * Creates a manager that allows to listen for device location updates.
 */
- (nullable YMKLocationManager *)createLocationManager;


/**
 * Creates a suspended LocationSimulator object with the given geometry.
 */
- (nullable YMKLocationSimulator *)createLocationSimulatorWithGeometry:(nonnull YMKPolyline *)geometry;


/// @cond EXCLUDE
/**
 * Creates a guidance.
 */
- (nullable YMKGuidanceGuide *)createGuide;
/// @endcond


/// @cond EXCLUDE
/**
 * Creates a suspended recorded simulator object with the given report.
 */
- (nullable YMKRecordedSimulator *)createRecordedSimulatorWithReportData:(nullable YRTReportData *)reportData;
/// @endcond


/// @cond EXCLUDE
/**
 * Creates a report factory.
 */
- (nullable YRTReportFactory *)createReportFactory;
/// @endcond


/// @cond EXCLUDE
/**
 * Creates a service that provides mass transit information for the
 * chosen transit station.
 */
- (nullable YMKMasstransitInfoService *)createMasstransitInfoService;
/// @endcond


/**
 * Creates an internal "window" object that is used to show the map.
 *
 * Do not call this method - it is for internal use only. To show the
 * map, please use the corresponding map "view" object.
 */
- (nullable YMKMapWindow *)createMapWindowWithPlatformView:(nullable YRTView *)platformView;


/**
 * @attention This feature is not available in the free MapKit version.
 *
 *
 * Returns a manager that handles cached resources offline.
 */
@property (nonatomic, readonly, nullable) YMKOfflineCacheManager *offlineCacheManager;

/**
 * Creates a service that allows to find a panorama closest to the
 * chosen point.
 */
- (nullable YMKPanoramaService *)createPanoramaService;


/**
 * Creates an internal "window" object needed to show the panorama
 * viewer.
 *
 * Do not call this method - it is for internal use only. To open the
 * panorama viewer, please use the corresponding panorama "view" object.
 */
- (nullable YMKPanoramaPlayer *)createPanoramaPlayerWithPlatformView:(nullable YRTView *)platformView;


/// @cond EXCLUDE
/**
 * Creates a manager that retrieves photos for specified businesses.
 */
- (nullable YMKPhotosManager *)createPhotosManager;
/// @endcond


/// @cond EXCLUDE
/**
 * Creates a manager that allows to retrieve and add business reviews.
 */
- (nullable YMKReviewsManager *)createReviewsManager;
/// @endcond


/// @cond EXCLUDE
/**
 * Creates a manager that allows to retrieve and add road events.
 */
- (nullable YMKRoadEventsManager *)createRoadEventsManager;
/// @endcond


/**
 * Creates a manager that builds driving routes using the specified
 * endpoints and other route parameters.
 */
- (nullable YMKDrivingRouter *)createDrivingRouter;


/**
 * Creates a manager that builds public transit routes using the origin
 * and destination points.
 */
- (nullable YMKMasstransitRouter *)createMasstransitRouter;


/**
 * Creates a manager that builds pedestrian routes using the origin and
 * destination points.
 */
- (nullable YMKPedestrianRouter *)createPedestrianRouter;


/**
 * Creates a manager that allows to search for various geographical
 * objects using a variety of parameters.
 */
- (nullable YMKSearchManager *)createSearchManagerWithSearchManagerType:(YMKSearchSearchManagerType)searchManagerType;


/// @cond EXCLUDE
/**
 * Creates a manager that allows to get ads in the search menu for
 * places that match the query.
 */
- (nullable YMKSearchAdvertMenuManager *)createAdvertMenuManagerWithAdvertPageId:(nonnull NSString *)advertPageId;
/// @endcond


/// @cond EXCLUDE
/**
 * Creates a route advert manager that allows to get ads for places of
 * interest along the route.
 */
- (nullable YMKSearchAdvertRouteManager *)createAdvertRouteManagerWithAdvertPageId:(nonnull NSString *)advertPageId;
/// @endcond


/// @cond EXCLUDE
/**
 * Creates a manager for getting ad icons.
 */
- (nullable YMKSearchBitmapDownloader *)createBitmapDownloader;
/// @endcond


/// @cond EXCLUDE
/**
 * Creates a manager for getting taxi ride info.
 */
- (nullable YMKTaxiManager *)createTaxiManager;
/// @endcond


/// @cond EXCLUDE
/**
 * Starts MapKit background services.
 */
- (void)startBackgroundServices;
/// @endcond


/// @cond EXCLUDE
/**
 * Stops MapKit background services.
 */
- (void)stopBackgroundServices;
/// @endcond


/// @cond EXCLUDE
/**
 * Returns an object that allows you to subscribe to the current car
 * state (parked or driving).
 */
@property (nonatomic, readonly, nullable) YMKMyCarStateManager *myCarStateManager;
/// @endcond

/**
 * Returns the version of the mapkit bundle.
 */
@property (nonatomic, readonly, nonnull) NSString *version;

/**
 * Tells if this object is valid or no. Any method called on an invalid
 * object will throw an exception. The object becomes invalid only on UI
 * thread, and only when its implementation depends on objects already
 * destroyed by now. Please refer to general docs about the interface for
 * details on its invalidation.
 */
@property (nonatomic, readonly, getter=isValid) BOOL valid;

@end

