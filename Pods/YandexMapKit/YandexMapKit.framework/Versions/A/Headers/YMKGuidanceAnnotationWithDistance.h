#import <YandexMapKit/YMKDrivingAnnotation.h>
#import <YandexMapKit/YMKLocalizedValue.h>

/// @cond EXCLUDE
@interface YMKGuidanceAnnotationWithDistance : NSObject

@property (nonatomic, readonly, nonnull) YMKDrivingAnnotation *annotation;

@property (nonatomic, readonly, nonnull) YMKLocalizedValue *distance;


+ (nonnull YMKGuidanceAnnotationWithDistance *)annotationWithDistanceWithAnnotation:(nonnull YMKDrivingAnnotation *)annotation
                                                                           distance:(nonnull YMKLocalizedValue *)distance;


@end
/// @endcond

