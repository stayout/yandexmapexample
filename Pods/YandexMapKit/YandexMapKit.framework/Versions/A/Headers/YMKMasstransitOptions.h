#import <YandexMapKit/YMKTimeOptions.h>

@class YMKMasstransitRouter;

/**
 * User-defined options for a YMKMasstransitRouter request.
 */
@interface YMKMasstransitOptions : NSObject

/**
 * Transport types that the router will avoid.
 */
@property (nonatomic, strong) NSArray<NSString *> *avoidTypes;

/**
 * Transport types that will be allowed even if they are avoided. The
 * client must explicitly add all known non-avoided vehicle types to
 * this parameter when sending a request to the mass transit router.
 * This is necessary to prevent problems with avoiding subtypes of any
 * avoided type if the user did not set them as avoided. The server
 * supports at least the following vehicle type identifiers: bus,
 * trolleybus, tramway, minibus, suburban, underground, ferry, cable,
 * funicular. You must put every vehicle type identifier known to you
 * either in avoidTypes or in acceptTypes list, so that router can
 * provide routes with the most accurate filtering options for your
 * application. Avoid making assumptions like vehicle type A is a
 * subtype of vehicle type B.
 */
@property (nonatomic, strong) NSArray<NSString *> *acceptTypes;

/**
 * Desired departure/arrival time settings. Empty YMKTimeOptions for
 * requests that are not time-dependent.
 */
@property (nonatomic, strong) YMKTimeOptions *timeOptions;

+ (nonnull YMKMasstransitOptions *)masstransitOptionsWithAvoidTypes:(nonnull NSArray<NSString *> *)avoidTypes
                                                        acceptTypes:(nonnull NSArray<NSString *> *)acceptTypes
                                                        timeOptions:(nonnull YMKTimeOptions *)timeOptions;


@end
