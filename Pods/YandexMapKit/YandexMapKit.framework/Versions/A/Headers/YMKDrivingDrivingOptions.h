#import <YandexMapKit/YMKDrivingDrivingRouter.h>

@interface YMKDrivingDrivingOptions : NSObject

/**
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSNumber *initialAzimuth;

/**
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSNumber *alternativeCount;

/**
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSNumber *offlineHandicap;

/**
 * The 'avoidTolls' option instructs the router to return routes that
 * avoid tolls, when possible.
 *
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSNumber *avoidTolls;

+ (nonnull YMKDrivingDrivingOptions *)drivingOptionsWithInitialAzimuth:(nullable NSNumber *)initialAzimuth
                                                      alternativeCount:(nullable NSNumber *)alternativeCount
                                                       offlineHandicap:(nullable NSNumber *)offlineHandicap
                                                            avoidTolls:(nullable NSNumber *)avoidTolls;


@end
