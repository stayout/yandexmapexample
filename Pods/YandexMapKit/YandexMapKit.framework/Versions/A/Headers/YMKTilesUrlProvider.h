#import <YandexMapKit/YMKTileId.h>
#import <YandexMapKit/YMKVersion.h>

@protocol YMKTilesUrlProvider <NSObject>

- (nonnull NSString *)formatUrlWithTileId:(nonnull YMKTileId *)tileId
                                  version:(nonnull YMKVersion *)version;


@end
