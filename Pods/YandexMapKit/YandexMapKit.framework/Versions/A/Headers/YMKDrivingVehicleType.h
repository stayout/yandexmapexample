#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, YMKDrivingVehicleType) {

    YMKDrivingVehicleTypeDefault,

    /// @cond EXCLUDE
    YMKDrivingVehicleTypeTaxi
    /// @endcond
};

