#import <YandexMapKit/YMKTaxiRideOption.h>

/// @cond EXCLUDE
@interface YMKTaxiRideInfo : NSObject

@property (nonatomic, readonly, nonnull) NSArray<YMKTaxiRideOption *> *rideOptions;


+ (nonnull YMKTaxiRideInfo *)rideInfoWithRideOptions:(nonnull NSArray<YMKTaxiRideOption *> *)rideOptions;


@end
/// @endcond

