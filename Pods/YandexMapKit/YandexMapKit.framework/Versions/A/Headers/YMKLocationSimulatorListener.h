#import <Foundation/Foundation.h>

/**
 * Listens for updates for location simulation.
 */
@protocol YMKLocationSimulatorListener <NSObject>

- (void)onSimulationFinished;


@end
