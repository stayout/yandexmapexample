#import <Foundation/Foundation.h>

/**
 * Driver actions.
 */
typedef NS_ENUM(NSUInteger, YMKDrivingAction) {

    YMKDrivingActionUnknown,

    YMKDrivingActionStraight,

    YMKDrivingActionSlightLeft,

    YMKDrivingActionSlightRight,

    YMKDrivingActionLeft,

    YMKDrivingActionRight,

    YMKDrivingActionHardLeft,

    YMKDrivingActionHardRight,

    YMKDrivingActionForkLeft,

    YMKDrivingActionForkRight,

    YMKDrivingActionUturnLeft,

    YMKDrivingActionUturnRight,

    YMKDrivingActionEnterRoundabout,

    YMKDrivingActionLeaveRoundabout,

    YMKDrivingActionBoardFerry,

    YMKDrivingActionLeaveFerry,

    YMKDrivingActionExitLeft,

    YMKDrivingActionExitRight,

    YMKDrivingActionFinish
};

