#import <Foundation/Foundation.h>

/**
 * Desired departure/arrival time settings. Only one the departureTime
 * and arrivalTime fields should be specified.
 */
@interface YMKTimeOptions : NSObject

/**
 * Desired departure time in UTC for a time-dependent route request. The
 * value is specified in milliseconds. This option cannot be used with
 * arrivalTime.
 *
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSDate *departureTime;

/**
 * Desired arrival time in UTC for a time-dependent route request. The
 * value is specified in milliseconds. This option cannot be used with
 * departureTime.
 *
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSDate *arrivalTime;

+ (nonnull YMKTimeOptions *)timeOptionsWithDepartureTime:(nullable NSDate *)departureTime
                                             arrivalTime:(nullable NSDate *)arrivalTime;


@end
