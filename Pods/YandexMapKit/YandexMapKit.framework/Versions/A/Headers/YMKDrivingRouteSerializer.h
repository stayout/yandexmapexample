#import <YandexRuntime/YRTPlatformBinding.h>

@class YMKDrivingRoute;

@interface YMKDrivingRouteSerializer : YRTPlatformBinding

- (nonnull NSData *)saveWithRoute:(nullable YMKDrivingRoute *)route;


/**
 * This method will return null if given a saved route from an
 * incompatible version of MapKit.
 */
- (nullable YMKDrivingRoute *)loadWithData:(nonnull NSData *)data;


@end

