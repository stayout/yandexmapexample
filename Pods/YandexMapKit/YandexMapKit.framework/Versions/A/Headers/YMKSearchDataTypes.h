#import <YandexMapKit/YMKSearchBusinessListObjectMetadata.h>
#import <YandexMapKit/YMKSearchBusinessPhotoObjectMetadata.h>
#import <YandexMapKit/YMKSearchBusinessRatingObjectMetadata.h>
#import <YandexMapKit/YMKSearchCurrencyExchangeMetadata.h>
#import <YandexMapKit/YMKSearchFuelMetadata.h>
#import <YandexMapKit/YMKSearchMassTransitObjectMetadata.h>
#import <YandexMapKit/YMKSearchPanoramasObjectMetadata.h>
#import <YandexMapKit/YMKSearchRelatedPlacesObjectMetadata.h>
#import <YandexMapKit/YMKSearchRouteDistancesObjectMetadata.h>
#import <YandexMapKit/YMKSearchRouterObjectMetadata.h>

/**
 * House number matching precision (response vs. request).
 */
typedef NS_ENUM(NSUInteger, YMKSearchPrecision) {

    /**
     * House number in the response is exactly the same as requested (3/2
     * vs. 3/2)
     */
    YMKSearchPrecisionExact,

    /**
     * House number in the response has the same number part as the
     * requested one (5 vs. 5a).
     */
    YMKSearchPrecisionNumber,

    /**
     * House number and coordinates are restored from the house range. This
     * means that there is no information about this specific house, but
     * there is information about range of houses to infer house position
     * from
     */
    YMKSearchPrecisionRange,

    /**
     * House number in the response is close to the requested one (13 vs.
     * 11).
     */
    YMKSearchPrecisionNearby
};


/**
 * Bitmask for requested search types.
 */
typedef NS_OPTIONS(NSUInteger, YMKSearchType) {

    /**
     * Default value: all types requested.
     */
    YMKSearchTypeNone = 0,

    /**
     * Toponyms.
     */
    YMKSearchTypeGeo = 1,

    /**
     * Companies.
     */
    YMKSearchTypeBiz = 1 << 1,

    /**
     * UGC data.
     */
    YMKSearchTypePsearch = 1 << 2,

    /**
     * Mass transit routes.
     */
    YMKSearchTypeTransit = 1 << 3
};


/**
 * Requested snippets bitmask.
 *
 * Snippets are additional pieces of information (possibly from
 * different services) which are not directly stored in object metadata
 * but may be requested separately based on client needs.
 *
 * Different snippets are applicable to different objects: some of the
 * snippets can be provided only for toponyms, some for businesses and
 * some for all object types.
 */
typedef NS_OPTIONS(NSUInteger, YMKSearchSnippet) {

    /**
     * Default value: no snippets requested.
     */
    YMKSearchSnippetNone = 0,

    /**
     * Related photos snippet (can be requested for a company or toponym).
     * See YMKSearchBusinessPhotoObjectMetadata.
     */
    YMKSearchSnippetPhotos = 1,

    /**
     * Ratings information (can be requested for a company). See
     * YMKSearchBusinessRatingObjectMetadata.
     */
    YMKSearchSnippetBusinessRating = 1 << 1,

    /**
     * Businesses at this address (can be requested for a toponym). See
     * YMKSearchBusinessListObjectMetadata.
     */
    YMKSearchSnippetBusinessList = 1 << 2,

    /**
     * Available routing types for this point. See
     * YMKSearchRouterObjectMetadata.
     */
    YMKSearchSnippetRouter = 1 << 3,

    /**
     * Nearest panoramas to this point. See
     * YMKSearchPanoramasObjectMetadata.
     */
    YMKSearchSnippetPanoramas = 1 << 4,

    /**
     * Nearest mass trasnsit stops to this point. See
     * YMKSearchMassTransitObjectMetadata.
     */
    YMKSearchSnippetMassTransit = 1 << 5,

    /**
     * Experimental snippets, shouldn't be used.
     */
    YMKSearchSnippetExperimental = 1 << 6,

    /**
     * Route times and distances for different transport types for this
     * point. See YMKSearchRouteDistancesObjectMetadata.
     */
    YMKSearchSnippetRouteDistances = 1 << 7,

    /**
     * Related businesses (can be requested for a businesses). See
     * YMKSearchRelatedPlacesObjectMetadata.
     */
    YMKSearchSnippetRelatedPlaces = 1 << 8,

    /**
     * Related images (can be requested for a business).
     */
    YMKSearchSnippetBusinessImages = 1 << 9,

    /**
     * References to external id's.
     */
    YMKSearchSnippetReferences = 1 << 10,

    /**
     * Fuel snippet. See YMKSearchFuelMetadata.
     */
    YMKSearchSnippetFuel = 1 << 11,

    /**
     * Currency exchange snippet. See YMKSearchCurrencyExchangeMetadata.
     */
    YMKSearchSnippetExchange = 1 << 12
};

