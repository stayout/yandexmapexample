#import <Foundation/Foundation.h>

/**
 * The description of the object.
 */
@interface YMKDrivingToponymPhrase : NSObject

/**
 * The string containing the description.
 */
@property (nonatomic, readonly, nonnull) NSString *text;


+ (nonnull YMKDrivingToponymPhrase *)toponymPhraseWithText:(nonnull NSString *)text;


@end

