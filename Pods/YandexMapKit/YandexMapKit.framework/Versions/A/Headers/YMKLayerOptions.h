#import <Foundation/Foundation.h>

/**
 * Options that are used when adding a layer to the map.
 */
@interface YMKLayerOptions : NSObject

/**
 * Inactive layers are not displayed on the map and do not request any
 * tiles from TileProvider.
 */
@property (nonatomic, assign) BOOL active;

/**
 * Indicates availability of night mode for this layer (for example,
 * night mode is disabled for the satellite layer). Default: true.
 */
@property (nonatomic, assign) BOOL nightModeAvailable;

/**
 * Determines whether tiles are cached on persistent storage or not.
 */
@property (nonatomic, assign) BOOL cacheable;

/**
 * <--!FIXME What for? I have Layer::invalidate. And I don't have
 * version at startup time-->
 *
 * Provide the version if you want your layer to be cached on disk.
 */
@property (nonatomic, copy) NSString *version_;

/**
 * Indicates whether layer activation should be animated.
 */
@property (nonatomic, assign) BOOL animateOnActivation;

+ (nonnull YMKLayerOptions *)layerOptionsWithActive:( BOOL)active
                                 nightModeAvailable:( BOOL)nightModeAvailable
                                          cacheable:( BOOL)cacheable
                                           version_:(nonnull NSString *)version_
                                animateOnActivation:( BOOL)animateOnActivation;


@end
