#import <YandexMapKit/YMKReviewsEraseSession.h>

#import <yandex/maps/mapkit/reviews/reviews_manager.h>

#import <memory>

namespace yandex {
namespace maps {
namespace mapkit {
namespace reviews {
namespace ios {

EraseSession::OnReviewsEraseCompleted onReviewsEraseCompleted(
    YMKReviewsEraseSessionHandler handler);
EraseSession::OnReviewsEraseError onReviewsEraseError(
    YMKReviewsEraseSessionHandler handler);

} // namespace ios
} // namespace reviews
} // namespace mapkit
} // namespace maps
} // namespace yandex

@interface YMKReviewsEraseSession ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::reviews::EraseSession>)native;

- (::yandex::maps::mapkit::reviews::EraseSession *)nativeEraseSession;

@end
