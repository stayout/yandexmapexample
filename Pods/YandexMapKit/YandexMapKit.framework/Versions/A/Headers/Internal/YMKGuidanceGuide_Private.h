#import <YandexMapKit/YMKGuidanceGuide.h>

#import <YandexRuntime/YRTSubscription.h>

#import <yandex/maps/mapkit/guidance/guide.h>
#import <yandex/maps/runtime/ios/object.h>

#import <memory>

@interface YMKGuidanceGuide ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::guidance::Guide>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::guidance::Guide>)nativeGuide;
- (std::shared_ptr<::yandex::maps::mapkit::guidance::Guide>)native;

@end
