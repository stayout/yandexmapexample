#import <YandexMapKit/YMKDrivingSummarySession.h>

#import <yandex/maps/mapkit/driving/session.h>

#import <memory>

namespace yandex {
namespace maps {
namespace mapkit {
namespace driving {
namespace ios {

SummarySession::OnDrivingSummaries onDrivingSummaries(
    YMKDrivingSummarySessionSummaryHandler handler);
SummarySession::OnDrivingSummariesError onDrivingSummariesError(
    YMKDrivingSummarySessionSummaryHandler handler);

} // namespace ios
} // namespace driving
} // namespace mapkit
} // namespace maps
} // namespace yandex

@interface YMKDrivingSummarySession ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::driving::SummarySession>)native;

- (::yandex::maps::mapkit::driving::SummarySession *)nativeSummarySession;

@end
