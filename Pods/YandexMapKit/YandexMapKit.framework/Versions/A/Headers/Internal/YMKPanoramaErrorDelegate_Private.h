#import <YandexMapKit/YMKPanoramaErrorDelegate.h>

#import <yandex/maps/mapkit/panorama/player.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace mapkit {
namespace panorama {
namespace ios {

class ErrorListenerBinding : public ::yandex::maps::mapkit::panorama::ErrorListener {
public:
    explicit ErrorListenerBinding(
        id<YMKPanoramaErrorDelegate> platformListener);

    virtual void onPanoramaOpenError(
        ::yandex::maps::mapkit::panorama::Player* player,
        ::yandex::maps::runtime::Error* error) override;

    id<YMKPanoramaErrorDelegate> platformReference() const { return platformListener_; }

private:
    __weak id<YMKPanoramaErrorDelegate> platformListener_;
};

} // namespace ios
} // namespace panorama
} // namespace mapkit
} // namespace maps
} // namespace yandex

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::panorama::ErrorListener>, id<YMKPanoramaErrorDelegate>, void> {
    static std::shared_ptr<::yandex::maps::mapkit::panorama::ErrorListener> from(
        id<YMKPanoramaErrorDelegate> platformErrorListener);
};
template <typename PlatformType>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::panorama::ErrorListener>, PlatformType> {
    static std::shared_ptr<::yandex::maps::mapkit::panorama::ErrorListener> from(
        PlatformType platformErrorListener)
    {
        return ToNative<std::shared_ptr<::yandex::maps::mapkit::panorama::ErrorListener>, id<YMKPanoramaErrorDelegate>>::from(
            platformErrorListener);
    }
};

template <>
struct ToPlatform<std::shared_ptr<::yandex::maps::mapkit::panorama::ErrorListener>> {
    static id<YMKPanoramaErrorDelegate> from(
        const std::shared_ptr<::yandex::maps::mapkit::panorama::ErrorListener>& nativeErrorListener);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
