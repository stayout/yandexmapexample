#import <YandexMapKit/YMKMasstransitSummarySession.h>

#import <yandex/maps/mapkit/masstransit/session.h>

#import <memory>

namespace yandex {
namespace maps {
namespace mapkit {
namespace masstransit {
namespace ios {

SummarySession::OnMasstransitSummaries onMasstransitSummaries(
    YMKMasstransitSummarySessionSummaryHandler handler);
SummarySession::OnMasstransitSummariesError onMasstransitSummariesError(
    YMKMasstransitSummarySessionSummaryHandler handler);

} // namespace ios
} // namespace masstransit
} // namespace mapkit
} // namespace maps
} // namespace yandex

@interface YMKMasstransitSummarySession ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::masstransit::SummarySession>)native;

- (::yandex::maps::mapkit::masstransit::SummarySession *)nativeSummarySession;

@end
