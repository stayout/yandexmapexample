#import <YandexMapKit/YMKMasstransitOptions.h>

#import <yandex/maps/mapkit/masstransit/masstransit_router.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::masstransit::MasstransitOptions, YMKMasstransitOptions, void> {
    static ::yandex::maps::mapkit::masstransit::MasstransitOptions from(
        YMKMasstransitOptions* platformMasstransitOptions);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::masstransit::MasstransitOptions, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKMasstransitOptions*>::value>::type> {
    static ::yandex::maps::mapkit::masstransit::MasstransitOptions from(
        PlatformType platformMasstransitOptions)
    {
        return ToNative<::yandex::maps::mapkit::masstransit::MasstransitOptions, YMKMasstransitOptions>::from(
            platformMasstransitOptions);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::masstransit::MasstransitOptions> {
    static YMKMasstransitOptions* from(
        const ::yandex::maps::mapkit::masstransit::MasstransitOptions& masstransitOptions);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
