#import <YandexMapKit/YMKGuidanceLocalizedPhrase.h>

#import <yandex/maps/mapkit/guidance/speaker.h>

#import <memory>

@interface YMKGuidanceLocalizedPhrase ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::guidance::LocalizedPhrase>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::guidance::LocalizedPhrase>)nativeLocalizedPhrase;
- (std::shared_ptr<::yandex::maps::mapkit::guidance::LocalizedPhrase>)native;

@end
