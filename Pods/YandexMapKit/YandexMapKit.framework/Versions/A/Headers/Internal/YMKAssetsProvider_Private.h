#import <YandexMapKit/YMKAssetsProvider.h>

#import <yandex/maps/mapkit/search_layer/assets_provider.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace mapkit {
namespace search_layer {
namespace ios {

class AssetsProviderBinding : public ::yandex::maps::mapkit::search_layer::AssetsProvider {
public:
    explicit AssetsProviderBinding(
        id<YMKAssetsProvider> platformListener);

    virtual std::unique_ptr<::yandex::maps::runtime::image::ImageProvider> image(
        ::yandex::maps::mapkit::search_layer::SearchResultItem* searchResult,
        ::yandex::maps::mapkit::search_layer::PlacemarkIconType type) override;

    virtual ::yandex::maps::mapkit::search_layer::Size size(
        ::yandex::maps::mapkit::search_layer::SearchResultItem* searchResult,
        ::yandex::maps::mapkit::search_layer::PlacemarkIconType type) override;

    virtual ::yandex::maps::mapkit::map::IconStyle iconStyle(
        ::yandex::maps::mapkit::search_layer::SearchResultItem* searchResult,
        ::yandex::maps::mapkit::search_layer::PlacemarkIconType type) override;

    id<YMKAssetsProvider> platformReference() const { return platformListener_; }

private:
    __weak id<YMKAssetsProvider> platformListener_;
};

} // namespace ios
} // namespace search_layer
} // namespace mapkit
} // namespace maps
} // namespace yandex

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::search_layer::AssetsProvider>, id<YMKAssetsProvider>, void> {
    static std::shared_ptr<::yandex::maps::mapkit::search_layer::AssetsProvider> from(
        id<YMKAssetsProvider> platformAssetsProvider);
};
template <typename PlatformType>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::search_layer::AssetsProvider>, PlatformType> {
    static std::shared_ptr<::yandex::maps::mapkit::search_layer::AssetsProvider> from(
        PlatformType platformAssetsProvider)
    {
        return ToNative<std::shared_ptr<::yandex::maps::mapkit::search_layer::AssetsProvider>, id<YMKAssetsProvider>>::from(
            platformAssetsProvider);
    }
};

template <>
struct ToPlatform<std::shared_ptr<::yandex::maps::mapkit::search_layer::AssetsProvider>> {
    static id<YMKAssetsProvider> from(
        const std::shared_ptr<::yandex::maps::mapkit::search_layer::AssetsProvider>& nativeAssetsProvider);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
