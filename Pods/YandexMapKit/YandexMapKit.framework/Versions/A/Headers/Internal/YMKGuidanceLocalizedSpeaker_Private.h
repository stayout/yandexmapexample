#import <YandexMapKit/YMKGuidanceLocalizedSpeaker.h>

#import <yandex/maps/mapkit/guidance/speaker.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace mapkit {
namespace guidance {
namespace ios {

class LocalizedSpeakerBinding : public ::yandex::maps::mapkit::guidance::LocalizedSpeaker {
public:
    explicit LocalizedSpeakerBinding(
        id<YMKGuidanceLocalizedSpeaker> platformListener);

    virtual void reset() override;

    virtual void say(
        std::shared_ptr<::yandex::maps::mapkit::guidance::LocalizedPhrase>& phrase) override;

    virtual double duration(
        std::shared_ptr<::yandex::maps::mapkit::guidance::LocalizedPhrase>& phrase) const override;

    id<YMKGuidanceLocalizedSpeaker> platformReference() const { return platformListener_; }

private:
    __weak id<YMKGuidanceLocalizedSpeaker> platformListener_;
};

} // namespace ios
} // namespace guidance
} // namespace mapkit
} // namespace maps
} // namespace yandex

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::guidance::LocalizedSpeaker>, id<YMKGuidanceLocalizedSpeaker>, void> {
    static std::shared_ptr<::yandex::maps::mapkit::guidance::LocalizedSpeaker> from(
        id<YMKGuidanceLocalizedSpeaker> platformLocalizedSpeaker);
};
template <typename PlatformType>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::guidance::LocalizedSpeaker>, PlatformType> {
    static std::shared_ptr<::yandex::maps::mapkit::guidance::LocalizedSpeaker> from(
        PlatformType platformLocalizedSpeaker)
    {
        return ToNative<std::shared_ptr<::yandex::maps::mapkit::guidance::LocalizedSpeaker>, id<YMKGuidanceLocalizedSpeaker>>::from(
            platformLocalizedSpeaker);
    }
};

template <>
struct ToPlatform<std::shared_ptr<::yandex::maps::mapkit::guidance::LocalizedSpeaker>> {
    static id<YMKGuidanceLocalizedSpeaker> from(
        const std::shared_ptr<::yandex::maps::mapkit::guidance::LocalizedSpeaker>& nativeLocalizedSpeaker);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
