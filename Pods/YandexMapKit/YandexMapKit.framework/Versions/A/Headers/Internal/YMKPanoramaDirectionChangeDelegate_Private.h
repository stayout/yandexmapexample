#import <YandexMapKit/YMKPanoramaDirectionChangeDelegate.h>

#import <yandex/maps/mapkit/panorama/player.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace mapkit {
namespace panorama {
namespace ios {

class DirectionChangeListenerBinding : public ::yandex::maps::mapkit::panorama::DirectionChangeListener {
public:
    explicit DirectionChangeListenerBinding(
        id<YMKPanoramaDirectionChangeDelegate> platformListener);

    virtual void onPanoramaDirectionChanged(
        ::yandex::maps::mapkit::panorama::Player* player) override;

    id<YMKPanoramaDirectionChangeDelegate> platformReference() const { return platformListener_; }

private:
    __weak id<YMKPanoramaDirectionChangeDelegate> platformListener_;
};

} // namespace ios
} // namespace panorama
} // namespace mapkit
} // namespace maps
} // namespace yandex

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::panorama::DirectionChangeListener>, id<YMKPanoramaDirectionChangeDelegate>, void> {
    static std::shared_ptr<::yandex::maps::mapkit::panorama::DirectionChangeListener> from(
        id<YMKPanoramaDirectionChangeDelegate> platformDirectionChangeListener);
};
template <typename PlatformType>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::panorama::DirectionChangeListener>, PlatformType> {
    static std::shared_ptr<::yandex::maps::mapkit::panorama::DirectionChangeListener> from(
        PlatformType platformDirectionChangeListener)
    {
        return ToNative<std::shared_ptr<::yandex::maps::mapkit::panorama::DirectionChangeListener>, id<YMKPanoramaDirectionChangeDelegate>>::from(
            platformDirectionChangeListener);
    }
};

template <>
struct ToPlatform<std::shared_ptr<::yandex::maps::mapkit::panorama::DirectionChangeListener>> {
    static id<YMKPanoramaDirectionChangeDelegate> from(
        const std::shared_ptr<::yandex::maps::mapkit::panorama::DirectionChangeListener>& nativeDirectionChangeListener);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
