#import <YandexMapKit/YMKDrivingRoute.h>
#import <YandexMapKit/YMKPolylinePosition.h>

@interface YMKDrivingInternalRouteUtils : NSObject

+ (NSArray *)getRequestPointsAfterPosition:(YMKPolylinePosition *)position
                                     route:(YMKDrivingRoute *)route;

+ (YMKDrivingRoute *)dropRouteViaPointsWithRoute:(YMKDrivingRoute *)route;

@end

