#import <YandexMapKit/YMKDrivingConditionsListener.h>

#import <yandex/maps/mapkit/driving/route.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace mapkit {
namespace driving {
namespace ios {

class ConditionsListenerBinding : public ::yandex::maps::mapkit::driving::ConditionsListener {
public:
    explicit ConditionsListenerBinding(
        id<YMKDrivingConditionsListener> platformListener);

    virtual void onConditionsUpdated() override;

    virtual void onConditionsOutdated() override;

    id<YMKDrivingConditionsListener> platformReference() const { return platformListener_; }

private:
    __weak id<YMKDrivingConditionsListener> platformListener_;
};

} // namespace ios
} // namespace driving
} // namespace mapkit
} // namespace maps
} // namespace yandex

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::driving::ConditionsListener>, id<YMKDrivingConditionsListener>, void> {
    static std::shared_ptr<::yandex::maps::mapkit::driving::ConditionsListener> from(
        id<YMKDrivingConditionsListener> platformConditionsListener);
};
template <typename PlatformType>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::driving::ConditionsListener>, PlatformType> {
    static std::shared_ptr<::yandex::maps::mapkit::driving::ConditionsListener> from(
        PlatformType platformConditionsListener)
    {
        return ToNative<std::shared_ptr<::yandex::maps::mapkit::driving::ConditionsListener>, id<YMKDrivingConditionsListener>>::from(
            platformConditionsListener);
    }
};

template <>
struct ToPlatform<std::shared_ptr<::yandex::maps::mapkit::driving::ConditionsListener>> {
    static id<YMKDrivingConditionsListener> from(
        const std::shared_ptr<::yandex::maps::mapkit::driving::ConditionsListener>& nativeConditionsListener);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
