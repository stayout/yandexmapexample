#import <YandexMapKit/YMKRecordedSimulator.h>

#import <YandexRuntime/YRTSubscription.h>

#import <yandex/maps/mapkit/guidance/recorded_simulator.h>
#import <yandex/maps/runtime/ios/object.h>

#import <memory>

@interface YMKRecordedSimulator ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::guidance::RecordedSimulator>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::guidance::RecordedSimulator>)nativeRecordedSimulator;

@end
