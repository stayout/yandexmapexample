#import <YandexMapKit/YMKTaxiManager.h>

#import <yandex/maps/mapkit/taxi/taxi_manager.h>

#import <memory>

@interface YMKTaxiManager ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::taxi::TaxiManager>)native;

- (::yandex::maps::mapkit::taxi::TaxiManager *)nativeTaxiManager;

@end
