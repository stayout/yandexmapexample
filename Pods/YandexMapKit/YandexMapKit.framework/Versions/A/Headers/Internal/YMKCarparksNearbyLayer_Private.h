#import <YandexMapKit/YMKCarparksNearbyLayer.h>

#import <yandex/maps/mapkit/carparks/carparks_nearby_layer.h>

#import <memory>

@interface YMKCarparksNearbyLayer ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::carparks::CarparksNearbyLayer>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::carparks::CarparksNearbyLayer>)nativeCarparksNearbyLayer;
- (std::shared_ptr<::yandex::maps::mapkit::carparks::CarparksNearbyLayer>)native;

@end
