#import <YandexMapKit/YMKOfflineCacheRegionFile.h>

#import <yandex/maps/mapkit/offline_cache/region_list.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::offline_cache::RegionFile, YMKOfflineCacheRegionFile, void> {
    static ::yandex::maps::mapkit::offline_cache::RegionFile from(
        YMKOfflineCacheRegionFile* platformRegionFile);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::offline_cache::RegionFile, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKOfflineCacheRegionFile*>::value>::type> {
    static ::yandex::maps::mapkit::offline_cache::RegionFile from(
        PlatformType platformRegionFile)
    {
        return ToNative<::yandex::maps::mapkit::offline_cache::RegionFile, YMKOfflineCacheRegionFile>::from(
            platformRegionFile);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::offline_cache::RegionFile> {
    static YMKOfflineCacheRegionFile* from(
        const ::yandex::maps::mapkit::offline_cache::RegionFile& regionFile);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
