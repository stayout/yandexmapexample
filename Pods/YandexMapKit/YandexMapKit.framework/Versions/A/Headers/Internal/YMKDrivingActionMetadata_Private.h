#import <YandexMapKit/YMKDrivingActionMetadata.h>

#import <yandex/maps/mapkit/driving/annotation.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::driving::ActionMetadata, YMKDrivingActionMetadata, void> {
    static ::yandex::maps::mapkit::driving::ActionMetadata from(
        YMKDrivingActionMetadata* platformActionMetadata);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::driving::ActionMetadata, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKDrivingActionMetadata*>::value>::type> {
    static ::yandex::maps::mapkit::driving::ActionMetadata from(
        PlatformType platformActionMetadata)
    {
        return ToNative<::yandex::maps::mapkit::driving::ActionMetadata, YMKDrivingActionMetadata>::from(
            platformActionMetadata);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::driving::ActionMetadata> {
    static YMKDrivingActionMetadata* from(
        const ::yandex::maps::mapkit::driving::ActionMetadata& actionMetadata);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
