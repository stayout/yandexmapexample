#import <YandexMapKit/YMKDrivingSession.h>

#import <yandex/maps/mapkit/driving/session.h>

#import <memory>

namespace yandex {
namespace maps {
namespace mapkit {
namespace driving {
namespace ios {

Session::OnDrivingRoutes onDrivingRoutes(
    YMKDrivingSessionRouteHandler handler);
Session::OnDrivingRoutesError onDrivingRoutesError(
    YMKDrivingSessionRouteHandler handler);

} // namespace ios
} // namespace driving
} // namespace mapkit
} // namespace maps
} // namespace yandex

@interface YMKDrivingSession ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::driving::Session>)native;

- (::yandex::maps::mapkit::driving::Session *)nativeSession;

@end
