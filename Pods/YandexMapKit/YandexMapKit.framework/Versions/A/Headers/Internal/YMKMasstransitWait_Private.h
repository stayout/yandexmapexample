#import <YandexMapKit/YMKMasstransitWait.h>

#import <yandex/maps/mapkit/masstransit/route.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::masstransit::Wait, YMKMasstransitWait, void> {
    static ::yandex::maps::mapkit::masstransit::Wait from(
        YMKMasstransitWait* platformWait);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::masstransit::Wait, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKMasstransitWait*>::value>::type> {
    static ::yandex::maps::mapkit::masstransit::Wait from(
        PlatformType platformWait)
    {
        return ToNative<::yandex::maps::mapkit::masstransit::Wait, YMKMasstransitWait>::from(
            platformWait);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::masstransit::Wait> {
    static YMKMasstransitWait* from(
        const ::yandex::maps::mapkit::masstransit::Wait& wait);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
