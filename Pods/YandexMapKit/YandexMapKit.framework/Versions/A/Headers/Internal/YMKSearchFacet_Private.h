#import <YandexMapKit/YMKSearchFacet.h>

#import <yandex/maps/mapkit/search/business_rating_object_metadata.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::search::Facet, YMKSearchFacet, void> {
    static ::yandex::maps::mapkit::search::Facet from(
        YMKSearchFacet* platformFacet);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::search::Facet, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKSearchFacet*>::value>::type> {
    static ::yandex::maps::mapkit::search::Facet from(
        PlatformType platformFacet)
    {
        return ToNative<::yandex::maps::mapkit::search::Facet, YMKSearchFacet>::from(
            platformFacet);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::search::Facet> {
    static YMKSearchFacet* from(
        const ::yandex::maps::mapkit::search::Facet& facet);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
