#import <YandexMapKit/YMKSearchLayer.h>

#import <YandexRuntime/YRTSubscription.h>

#import <yandex/maps/mapkit/search_layer/search_layer.h>
#import <yandex/maps/runtime/ios/object.h>

#import <memory>

@interface YMKSearchLayer ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::search_layer::SearchLayer>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::search_layer::SearchLayer>)nativeSearchLayer;
- (std::shared_ptr<::yandex::maps::mapkit::search_layer::SearchLayer>)native;

@end
