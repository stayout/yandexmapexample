#import <YandexMapKit/YMKPedestrianRouter.h>

#import <yandex/maps/mapkit/masstransit/pedestrian_router.h>

#import <memory>

@interface YMKPedestrianRouter ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::masstransit::PedestrianRouter>)native;

- (::yandex::maps::mapkit::masstransit::PedestrianRouter *)nativePedestrianRouter;

@end
