#import <YandexMapKit/YMKManeuverStyle.h>

#import <yandex/maps/mapkit/map/route_helper.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::map::ManeuverStyle, YMKManeuverStyle, void> {
    static ::yandex::maps::mapkit::map::ManeuverStyle from(
        YMKManeuverStyle* platformManeuverStyle);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::map::ManeuverStyle, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKManeuverStyle*>::value>::type> {
    static ::yandex::maps::mapkit::map::ManeuverStyle from(
        PlatformType platformManeuverStyle)
    {
        return ToNative<::yandex::maps::mapkit::map::ManeuverStyle, YMKManeuverStyle>::from(
            platformManeuverStyle);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::map::ManeuverStyle> {
    static YMKManeuverStyle* from(
        const ::yandex::maps::mapkit::map::ManeuverStyle& maneuverStyle);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
