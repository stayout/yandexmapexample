#import <YandexMapKit/YMKPhotosImageSession.h>

#import <yandex/maps/mapkit/photos/photos_manager.h>

#import <memory>

namespace yandex {
namespace maps {
namespace mapkit {
namespace photos {
namespace ios {

ImageSession::OnImageReceived onImageReceived(
    YMKPhotosImageSessionHandler handler);
ImageSession::OnImageError onImageError(
    YMKPhotosImageSessionHandler handler);

} // namespace ios
} // namespace photos
} // namespace mapkit
} // namespace maps
} // namespace yandex

@interface YMKPhotosImageSession ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::photos::ImageSession>)native;

- (::yandex::maps::mapkit::photos::ImageSession *)nativeImageSession;

@end
