#import <YandexMapKit/YMKMasstransitBriefSchedule.h>

#import <yandex/maps/mapkit/masstransit/stop.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>





namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::masstransit::BriefSchedule::ScheduleEntry::Estimation, YMKMasstransitScheduleEntryEstimation, void> {
    static ::yandex::maps::mapkit::masstransit::BriefSchedule::ScheduleEntry::Estimation from(
        YMKMasstransitScheduleEntryEstimation* platformEstimation);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::masstransit::BriefSchedule::ScheduleEntry::Estimation, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKMasstransitScheduleEntryEstimation*>::value>::type> {
    static ::yandex::maps::mapkit::masstransit::BriefSchedule::ScheduleEntry::Estimation from(
        PlatformType platformEstimation)
    {
        return ToNative<::yandex::maps::mapkit::masstransit::BriefSchedule::ScheduleEntry::Estimation, YMKMasstransitScheduleEntryEstimation>::from(
            platformEstimation);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::masstransit::BriefSchedule::ScheduleEntry::Estimation> {
    static YMKMasstransitScheduleEntryEstimation* from(
        const ::yandex::maps::mapkit::masstransit::BriefSchedule::ScheduleEntry::Estimation& estimation);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex



namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::masstransit::BriefSchedule::ScheduleEntry::Scheduled, YMKMasstransitScheduleEntryScheduled, void> {
    static ::yandex::maps::mapkit::masstransit::BriefSchedule::ScheduleEntry::Scheduled from(
        YMKMasstransitScheduleEntryScheduled* platformScheduled);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::masstransit::BriefSchedule::ScheduleEntry::Scheduled, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKMasstransitScheduleEntryScheduled*>::value>::type> {
    static ::yandex::maps::mapkit::masstransit::BriefSchedule::ScheduleEntry::Scheduled from(
        PlatformType platformScheduled)
    {
        return ToNative<::yandex::maps::mapkit::masstransit::BriefSchedule::ScheduleEntry::Scheduled, YMKMasstransitScheduleEntryScheduled>::from(
            platformScheduled);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::masstransit::BriefSchedule::ScheduleEntry::Scheduled> {
    static YMKMasstransitScheduleEntryScheduled* from(
        const ::yandex::maps::mapkit::masstransit::BriefSchedule::ScheduleEntry::Scheduled& scheduled);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
