#import <YandexMapKit/YMKReviewsManager.h>

#import <yandex/maps/mapkit/reviews/reviews_manager.h>

#import <memory>

@interface YMKReviewsManager ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::reviews::ReviewsManager>)native;

- (::yandex::maps::mapkit::reviews::ReviewsManager *)nativeReviewsManager;

@end
