#import <YandexMapKit/YMKDrivingAnnotationLang.h>
#import <YandexMapKit/YMKPhraseTokens.h>

#import <YandexRuntime/YRTPlatformBinding.h>

/// @cond EXCLUDE
@interface YMKGuidanceLocalizedPhrase : YRTPlatformBinding

- (NSUInteger)tokensCount;


- (YMKPhraseToken)tokenWithIndex:(NSUInteger)index;


- (nonnull NSString *)text;


- (YMKDrivingAnnotationLanguage)language;


@end
/// @endcond

