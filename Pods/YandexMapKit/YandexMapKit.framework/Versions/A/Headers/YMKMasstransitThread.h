#import <YandexMapKit/YMKMasstransitLine.h>
#import <YandexMapKit/YMKMasstransitStop.h>

/**
 * Describes a public transport thread. A thread is one of the
 * YMKMasstransitLine variants. For example, one line can have two
 * threads: direct and return.
 */
@interface YMKMasstransitThread : NSObject

/**
 * Thread ID.
 */
@property (nonatomic, readonly, nonnull) NSString *id;

/**
 * List of important stops on the thread, such as the first and last
 * stops.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKMasstransitStop *> *essentialStops;


+ (nonnull YMKMasstransitThread *)threadWithId:(nonnull NSString *)id
                                essentialStops:(nonnull NSArray<YMKMasstransitStop *> *)essentialStops;


@end

