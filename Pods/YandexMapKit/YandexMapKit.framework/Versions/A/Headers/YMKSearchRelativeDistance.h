#import <YandexMapKit/YMKSearchTravelInfo.h>

/**
 * Describes relative distance info.
 */
@interface YMKSearchRelativeDistance : NSObject

/**
 * Travel info for driving route.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKSearchTravelInfo *driving;

/**
 * Travel info for walking route.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKSearchTravelInfo *walking;


+ (nonnull YMKSearchRelativeDistance *)relativeDistanceWithDriving:(nullable YMKSearchTravelInfo *)driving
                                                           walking:(nullable YMKSearchTravelInfo *)walking;


@end

