#import <YandexMapKit/YMKAttribution.h>

@class YMKSearchFeatureEnumValue;
@class YMKSearchFeatureVariantValue;

/**
 * Describes some common feature of organizations. Can be of three
 * types: - boolean (like on/off switch, as for free Wi-Fi availability)
 * - enumerated (can have multiple values at once, like cuisine types in
 * a cafe) - text (like enumerated but with any strings instead of
 * predefined values)
 */
@interface YMKSearchFeature : NSObject

/**
 * Machine readable feature identifier.
 */
@property (nonatomic, readonly, nonnull) NSString *id;

/**
 * Feature value (depends on feature type).
 */
@property (nonatomic, readonly, nonnull) YMKSearchFeatureVariantValue *value;

/**
 * Human readable localized representation.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *name;

/**
 * Reference to information source providing given feature (see
 * YMKAttribution)
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *aref;


+ (nonnull YMKSearchFeature *)featureWithId:(nonnull NSString *)id
                                      value:(nonnull YMKSearchFeatureVariantValue *)value
                                       name:(nullable NSString *)name
                                       aref:(nullable NSString *)aref;


@end


/**
 * Value for enumerated features.
 */
@interface YMKSearchFeatureEnumValue : NSObject

/**
 * Machine readable value identifier.
 */
@property (nonatomic, readonly, nonnull) NSString *id;

/**
 * Human readable localized representation.
 */
@property (nonatomic, readonly, nonnull) NSString *name;


+ (nonnull YMKSearchFeatureEnumValue *)enumValueWithId:(nonnull NSString *)id
                                                  name:(nonnull NSString *)name;


@end


@interface YMKSearchFeatureVariantValue : NSObject

@property (nonatomic, readonly) NSNumber *booleanValue;

@property (nonatomic, readonly) NSArray<NSString *> *textValue;

@property (nonatomic, readonly) NSArray<YMKSearchFeatureEnumValue *> *enumValue;

+ (YMKSearchFeatureVariantValue *)variantValueWithBooleanValue:(BOOL)booleanValue;

+ (YMKSearchFeatureVariantValue *)variantValueWithTextValue:(NSArray<NSString *> *)textValue;

+ (YMKSearchFeatureVariantValue *)variantValueWithEnumValue:(NSArray<YMKSearchFeatureEnumValue *> *)enumValue;

@end

