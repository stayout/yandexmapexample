#import <YandexMapKit/YMKLocalizedValue.h>
#import <YandexMapKit/YMKMasstransitStop.h>
#import <YandexMapKit/YMKTime.h>
#import <YandexMapKit/YMKVehicle.h>

@class YMKMasstransitBriefScheduleScheduleEntry;
@class YMKMasstransitScheduleEntryEstimation;
@class YMKMasstransitScheduleEntryPeriodical;
@class YMKMasstransitScheduleEntryScheduled;

/// @cond EXCLUDE
/**
 * @brief Contains information about upcoming vehicle arrivals and
 * departures for the specified YMKMasstransitStop.
 */
@interface YMKMasstransitBriefSchedule : NSObject

/**
 * Collection of schedule entries, sorted by time.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKMasstransitBriefScheduleScheduleEntry *> *scheduleEntries;


+ (nonnull YMKMasstransitBriefSchedule *)briefScheduleWithScheduleEntries:(nonnull NSArray<YMKMasstransitBriefScheduleScheduleEntry *> *)scheduleEntries;


@end
/// @endcond


/**
 * Describes the thread schedule at the specified time period.
 * ScheduleEntry either defines arrival and departure frequency for some
 * time range, or defines a single arrival and departure at some point
 * in time.
 */
@interface YMKMasstransitBriefScheduleScheduleEntry : NSObject

/**
 * Defines a frequency-based schedule that does not contain exact
 * arrival and departure times. A ScheduleEntry must have exactly one of
 * Periodical or Scheduled set.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKMasstransitScheduleEntryPeriodical *periodical;

/**
 * Defines the exact arrival and departure time. A ScheduleEntry must
 * have exactly one of Periodical or Scheduled set.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKMasstransitScheduleEntryScheduled *scheduled;


+ (nonnull YMKMasstransitBriefScheduleScheduleEntry *)scheduleEntryWithPeriodical:(nullable YMKMasstransitScheduleEntryPeriodical *)periodical
                                                                        scheduled:(nullable YMKMasstransitScheduleEntryScheduled *)scheduled;


@end


/**
 * Arrival time estimation.
 */
@interface YMKMasstransitScheduleEntryEstimation : NSObject

/**
 * Specifies which YMKVehicle is expected to arrive at the specified
 * time.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *vehicleId;

/**
 * Estimated arrival time.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKTime *arrivalTime;


+ (nonnull YMKMasstransitScheduleEntryEstimation *)estimationWithVehicleId:(nullable NSString *)vehicleId
                                                               arrivalTime:(nullable YMKTime *)arrivalTime;


@end


/**
 * Defines a frequency-based schedule that does not contain exact
 * arrival and departure times.
 */
@interface YMKMasstransitScheduleEntryPeriodical : NSObject

/**
 * Frequency at which vehicles arrive or depart on average.
 */
@property (nonatomic, readonly, nonnull) YMKLocalizedValue *frequency;

/**
 * Start of the time range when the ScheduleEntry is applicable.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKTime *begin;

/**
 * End of the time range when the ScheduleEntry is applicable.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKTime *end;

/**
 * Collection of estimated arrivals within the applicable time range.
 * The estimations are sorted by arrival time.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKMasstransitScheduleEntryEstimation *> *estimations;


+ (nonnull YMKMasstransitScheduleEntryPeriodical *)periodicalWithFrequency:(nonnull YMKLocalizedValue *)frequency
                                                                     begin:(nullable YMKTime *)begin
                                                                       end:(nullable YMKTime *)end
                                                               estimations:(nonnull NSArray<YMKMasstransitScheduleEntryEstimation *> *)estimations;


@end


/**
 * Defines the exact arrival and departure time.
 */
@interface YMKMasstransitScheduleEntryScheduled : NSObject

/**
 * Arrival time, if specified in schedule.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKTime *arrivalTime;

/**
 * Departure time, if specified in schedule.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKTime *departureTime;

/**
 * Estimated arrival time.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKMasstransitScheduleEntryEstimation *estimation;


+ (nonnull YMKMasstransitScheduleEntryScheduled *)scheduledWithArrivalTime:(nullable YMKTime *)arrivalTime
                                                             departureTime:(nullable YMKTime *)departureTime
                                                                estimation:(nullable YMKMasstransitScheduleEntryEstimation *)estimation;


@end

