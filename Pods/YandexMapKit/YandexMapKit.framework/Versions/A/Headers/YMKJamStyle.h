#import <YandexMapKit/YMKJamTypeColor.h>

@interface YMKJamStyle : NSObject

/**
 * Collection of colors for traffic intensity.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKJamTypeColor *> *colors;


+ (nonnull YMKJamStyle *)jamStyleWithColors:(nonnull NSArray<YMKJamTypeColor *> *)colors;


@end

