#import <YandexMapKit/YMKLocalizedValue.h>

/**
 * Describes information about route via masstransit.
 */
@interface YMKSearchTransitInfo : NSObject

/**
 * Route  duration.
 */
@property (nonatomic, readonly, nonnull) YMKLocalizedValue *duration;

/**
 * Number of transfers for the route. This includes both transfers
 * inside single type of transit (i.e. change from one underground line
 * to another) and transfers between different transit types (i.e.
 * transfer from bus to underground).
 */
@property (nonatomic, readonly) NSInteger transferCount;


+ (nonnull YMKSearchTransitInfo *)transitInfoWithDuration:(nonnull YMKLocalizedValue *)duration
                                            transferCount:( NSInteger)transferCount;


@end

