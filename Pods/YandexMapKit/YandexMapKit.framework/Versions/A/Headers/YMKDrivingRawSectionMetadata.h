#import <YandexMapKit/YMKDrivingAnnotation.h>
#import <YandexMapKit/YMKDrivingRawAnnotationSchemes.h>
#import <YandexMapKit/YMKDrivingRawJams.h>
#import <YandexMapKit/YMKDrivingRawLaneSigns.h>
#import <YandexMapKit/YMKDrivingRawSpeedLimits.h>
#import <YandexMapKit/YMKDrivingRawSpots.h>
#import <YandexMapKit/YMKDrivingWeight.h>

@interface YMKDrivingRawSectionMetadata : NSObject

@property (nonatomic, readonly) NSUInteger legIndex;

@property (nonatomic, readonly, nonnull) YMKDrivingWeight *weight;

@property (nonatomic, readonly, nonnull) YMKDrivingAnnotation *annotation;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *viaPointPositions;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKDrivingRawSpeedLimits *speedLimits;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKDrivingRawAnnotationSchemes *annotationSchemes;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKDrivingRawLaneSigns *laneSigns;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKDrivingRawSpots *spots;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKDrivingRawJams *jams;


+ (nonnull YMKDrivingRawSectionMetadata *)rawSectionMetadataWithLegIndex:( NSUInteger)legIndex
                                                                  weight:(nonnull YMKDrivingWeight *)weight
                                                              annotation:(nonnull YMKDrivingAnnotation *)annotation
                                                       viaPointPositions:(nonnull NSArray<NSNumber *> *)viaPointPositions
                                                             speedLimits:(nullable YMKDrivingRawSpeedLimits *)speedLimits
                                                       annotationSchemes:(nullable YMKDrivingRawAnnotationSchemes *)annotationSchemes
                                                               laneSigns:(nullable YMKDrivingRawLaneSigns *)laneSigns
                                                                   spots:(nullable YMKDrivingRawSpots *)spots
                                                                    jams:(nullable YMKDrivingRawJams *)jams;


@end

