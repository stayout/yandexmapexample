#import <YandexMapKit/YMKDrivingLaneSign.h>
#import <YandexMapKit/YMKGuidanceAnnotationWithDistance.h>

/// @cond EXCLUDE
@interface YMKGuidanceDisplayedAnnotations : NSObject

@property (nonatomic, readonly, nonnull) NSArray<YMKGuidanceAnnotationWithDistance *> *annotations;

/**
 * The name of the road we're about to turn onto. Never appears in
 * free-driving mode.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *nextRoadName;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKDrivingLaneSign *laneSign;


+ (nonnull YMKGuidanceDisplayedAnnotations *)displayedAnnotationsWithAnnotations:(nonnull NSArray<YMKGuidanceAnnotationWithDistance *> *)annotations
                                                                    nextRoadName:(nullable NSString *)nextRoadName
                                                                        laneSign:(nullable YMKDrivingLaneSign *)laneSign;


@end
/// @endcond

