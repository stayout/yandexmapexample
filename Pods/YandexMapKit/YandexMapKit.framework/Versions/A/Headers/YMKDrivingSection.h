#import <YandexMapKit/YMKDrivingSectionMetadata.h>
#import <YandexMapKit/YMKSubpolyline.h>

@interface YMKDrivingSection : NSObject

@property (nonatomic, readonly, nonnull) YMKDrivingSectionMetadata *metadata;

@property (nonatomic, readonly, nonnull) YMKSubpolyline *geometry;


+ (nonnull YMKDrivingSection *)sectionWithMetadata:(nonnull YMKDrivingSectionMetadata *)metadata
                                          geometry:(nonnull YMKSubpolyline *)geometry;


@end

