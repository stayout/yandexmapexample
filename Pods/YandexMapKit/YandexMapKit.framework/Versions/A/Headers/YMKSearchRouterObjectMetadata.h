#import <YandexMapKit/YMKSearchRouter.h>

/**
 * Snippet data to get router info.
 */
@interface YMKSearchRouterObjectMetadata : NSObject

/**
 * List of routers.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKSearchRouter *> *routers;


+ (nonnull YMKSearchRouterObjectMetadata *)routerObjectMetadataWithRouters:(nonnull NSArray<YMKSearchRouter *> *)routers;


@end

