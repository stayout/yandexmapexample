#import <YandexMapKit/YMKObjectEvent.h>
#import <YandexMapKit/YMKUserLocation.h>

/**
 * This event triggers when the user location icon type is changed.
 */
@interface YMKUserLocationIconChanged : YMKObjectEvent

@property (nonatomic, readonly) YMKUserLocationIconType iconType;

@end

