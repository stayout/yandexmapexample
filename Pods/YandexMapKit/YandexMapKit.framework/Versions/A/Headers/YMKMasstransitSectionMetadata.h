#import <YandexMapKit/YMKMasstransitTransfer.h>
#import <YandexMapKit/YMKMasstransitTransport.h>
#import <YandexMapKit/YMKMasstransitTravelEstimation.h>
#import <YandexMapKit/YMKMasstransitWait.h>
#import <YandexMapKit/YMKMasstransitWalk.h>
#import <YandexMapKit/YMKMasstransitWeight.h>

@class YMKMasstransitSectionMetadataSectionData;

/**
 * General information about a section of a route. The field
 * YMKMasstransitSectionMetadata::data describes the type of section:
 * wait, walk, transfer or transport and related data. Related data can
 * be set for walk and transfer sections: this data is a vector of
 * construction types of corresponding geometry segments.
 */
@interface YMKMasstransitSectionMetadata : NSObject

/**
 * Contains the route time, distance of the walking part, and the number
 * of transfers.
 */
@property (nonatomic, readonly, nonnull) YMKMasstransitWeight *weight;

/**
 * @brief Contains information that is specific to a section type: wait,
 * walk, transfer, or ride transport.
 */
@property (nonatomic, readonly, nonnull) YMKMasstransitSectionMetadataSectionData *data;

/**
 * Arrival and departure time estimations. This field is set only for
 * time-dependent routes.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKMasstransitTravelEstimation *estimation;


+ (nonnull YMKMasstransitSectionMetadata *)sectionMetadataWithWeight:(nonnull YMKMasstransitWeight *)weight
                                                                data:(nonnull YMKMasstransitSectionMetadataSectionData *)data
                                                          estimation:(nullable YMKMasstransitTravelEstimation *)estimation;


@end


@interface YMKMasstransitSectionMetadataSectionData : NSObject

@property (nonatomic, readonly) YMKMasstransitWait *wait;

@property (nonatomic, readonly) YMKMasstransitWalk *walk;

@property (nonatomic, readonly) YMKMasstransitTransfer *transfer;

@property (nonatomic, readonly) NSArray<YMKMasstransitTransport *> *transports;

+ (YMKMasstransitSectionMetadataSectionData *)sectionDataWithWait:(YMKMasstransitWait *)wait;

+ (YMKMasstransitSectionMetadataSectionData *)sectionDataWithWalk:(YMKMasstransitWalk *)walk;

+ (YMKMasstransitSectionMetadataSectionData *)sectionDataWithTransfer:(YMKMasstransitTransfer *)transfer;

+ (YMKMasstransitSectionMetadataSectionData *)sectionDataWithTransports:(NSArray<YMKMasstransitTransport *> *)transports;

@end

