#import <Foundation/Foundation.h>

/// @cond EXCLUDE
/**
 * Review facet, should be valued independently.
 */
@interface YMKReviewsReviewsTag : NSObject

/**
 * Facet ID.
 */
@property (nonatomic, readonly, nonnull) NSString *name;

/**
 * Organization's rating by this facet.
 */
@property (nonatomic, readonly) NSInteger value;

/**
 * Displayed facet name.
 */
@property (nonatomic, readonly, nonnull) NSString *caption;


+ (nonnull YMKReviewsReviewsTag *)reviewsTagWithName:(nonnull NSString *)name
                                               value:( NSInteger)value
                                             caption:(nonnull NSString *)caption;


@end
/// @endcond

