#import <YandexMapKit/YMKMasstransitStop.h>
#import <YandexMapKit/YMKPoint.h>

@class YMKMasstransitRoute;

/**
 * Describes a YMKMasstransitStop on a YMKMasstransitRoute.
 */
@interface YMKMasstransitRouteStop : NSObject

/**
 * Stop on a route.
 */
@property (nonatomic, readonly, nonnull) YMKMasstransitStop *stop;

/**
 * Coordinates of the stop.
 */
@property (nonatomic, readonly, nonnull) YMKPoint *position;


+ (nonnull YMKMasstransitRouteStop *)routeStopWithStop:(nonnull YMKMasstransitStop *)stop
                                              position:(nonnull YMKPoint *)position;


@end

