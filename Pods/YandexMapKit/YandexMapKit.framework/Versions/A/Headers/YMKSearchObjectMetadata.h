#import <Foundation/Foundation.h>

/**
 * General snippet.
 */
@interface YMKSearchObjectMetadata : NSObject

/**
 * Server-generated log identifier.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *logId;


+ (nonnull YMKSearchObjectMetadata *)searchObjectMetadataWithLogId:(nullable NSString *)logId;


@end

