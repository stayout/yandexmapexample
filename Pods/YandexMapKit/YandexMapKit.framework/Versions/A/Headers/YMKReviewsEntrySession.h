#import <YandexMapKit/YMKReviewsEntry.h>

#import <YandexRuntime/YRTPlatformBinding.h>

/// @cond EXCLUDE
typedef void(^YMKReviewsEntrySessionDataHandler)(
    YMKReviewsEntry *entry,
    NSError *error);

/**
 * Provides a particular review entry (review or review template).
 */
@interface YMKReviewsEntrySession : YRTPlatformBinding

/**
 * Cancels the pending operation, if there is one.
 */
- (void)cancel;


/**
 * Re-runs the operation with the same parameters, but a new listener.
 */
- (void)retryWithDataHandler:(nullable YMKReviewsEntrySessionDataHandler)dataHandler;


@end
/// @endcond

