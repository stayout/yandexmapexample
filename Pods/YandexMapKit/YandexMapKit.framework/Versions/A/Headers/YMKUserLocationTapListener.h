#import <YandexMapKit/YMKPoint.h>

/**
 * Triggered by a tap on the user location icon.
 */
@protocol YMKUserLocationTapListener <NSObject>

- (void)onUserLocationObjectTapWithPoint:(nonnull YMKPoint *)point;


@end
