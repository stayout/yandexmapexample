#import <YandexMapKit/YMKPoint.h>
#import <YandexMapKit/YMKTaxiRideInfoSession.h>

#import <YandexRuntime/YRTPlatformBinding.h>

/// @cond EXCLUDE
@interface YMKTaxiManager : YRTPlatformBinding

/**
 * Begin async ride info request
 *
 * @param startPoint to request information
 * @param endPoint to request information
 * @param rideInfoListener listener for result or error
 */
- (nullable YMKTaxiRideInfoSession *)requestRideInfoWithStartPoint:(nonnull YMKPoint *)startPoint
                                                          endPoint:(nonnull YMKPoint *)endPoint
                                                   responseHandler:(nullable YMKTaxiRideInfoSessionResponseHandler)responseHandler;


@end
/// @endcond

