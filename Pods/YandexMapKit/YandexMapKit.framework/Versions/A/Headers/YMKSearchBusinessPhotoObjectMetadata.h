#import <Foundation/Foundation.h>

@class YMKSearchBusinessPhotoObjectMetadataPhoto;

/// @cond EXCLUDE
/**
 * Snippet for company-related photos (becoming obsolete).
 */
@interface YMKSearchBusinessPhotoObjectMetadata : NSObject

/**
 * Number of photos for the organisation. (see PhotosManager for
 * details)
 */
@property (nonatomic, readonly) NSUInteger count;

/**
 * List of photos for the company (usually first three)
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKSearchBusinessPhotoObjectMetadataPhoto *> *photos;


+ (nonnull YMKSearchBusinessPhotoObjectMetadata *)businessPhotoObjectMetadataWithCount:( NSUInteger)count
                                                                                photos:(nonnull NSArray<YMKSearchBusinessPhotoObjectMetadataPhoto *> *)photos;


@end
/// @endcond


/**
 * Information about single photos.
 */
@interface YMKSearchBusinessPhotoObjectMetadataPhoto : NSObject

/**
 * Available sizes are listed here
 * https://tech.yandex.ru/fotki/doc/format-ref/f-img-docpage/ .
 */
@property (nonatomic, readonly, nonnull) NSString *id;


+ (nonnull YMKSearchBusinessPhotoObjectMetadataPhoto *)photoWithId:(nonnull NSString *)id;


@end

