#import <YandexMapKit/YMKPoint.h>

#import <YandexRuntime/YRTPlatformBinding.h>

@class YMKPanoramaServiceSearchSession;

typedef void(^YMKPanoramaServiceSearchHandler)(
    NSString *panoramaId,
    NSError *error);

@interface YMKPanoramaService : YRTPlatformBinding

/**
 * Requests the ID of the panorama that is closest to the specified
 * position.
 *
 * @param position Position to find the nearest panoramaId to.
 * @param searchListener Receives the panorama search result.
 *
 * @return Session handle that should be stored until searchListener is
 * notified.
 */
- (nullable YMKPanoramaServiceSearchSession *)findNearestWithPosition:(nonnull YMKPoint *)position
                                                        searchHandler:(nullable YMKPanoramaServiceSearchHandler)searchHandler;


@end


/**
 * Session for receiving the result of the findNearest() method.
 *
 * - Should be stored until the listener is notified. - Can be used to
 * cancel the active request. - Can be used to retry the last request
 * (for example, if it failed).
 */
@interface YMKPanoramaServiceSearchSession : YRTPlatformBinding

- (void)retryWithSearchHandler:(nullable YMKPanoramaServiceSearchHandler)searchHandler;


- (void)cancel;


@end

