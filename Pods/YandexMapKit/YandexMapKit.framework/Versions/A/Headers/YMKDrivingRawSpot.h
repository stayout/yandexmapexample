#import <YandexMapKit/YMKDrivingSpot.h>

@interface YMKDrivingRawSpot : NSObject

@property (nonatomic, readonly) YMKDrivingSpotType type;

@property (nonatomic, readonly) NSUInteger position;


+ (nonnull YMKDrivingRawSpot *)rawSpotWithType:( YMKDrivingSpotType)type
                                      position:( NSUInteger)position;


@end

