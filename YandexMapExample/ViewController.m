//
//  ViewController.m
//  YandexMapExample
//
//  Created by Stayout on 04.04.2018.
//  Copyright © 2018 WS. All rights reserved.
//

#import "ViewController.h"
#import <YandexMapKit/YMKMapKitFactory.h>
#import "WSPoint.h"
#import <CoreLocation/CoreLocation.h>
#import "NSStringCategory.h"

@interface ViewController () <YMKUserLocationObjectListener, YMKMapInputListener, YMKMapObjectTapListener, CLLocationManagerDelegate>
    
@property (weak, nonatomic) IBOutlet YMKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *actionView;

@property (strong, nonatomic) YMKSearchManager* searchManager;
@property (nonatomic) YMKSearchSession* searchSession;
@property (nonatomic) CLLocationManager* locationManager;
@property (nonatomic) UIImage* userLocationIcon;
@property (nonatomic) NSArray<WSPoint*>* addresses;
@property (nonatomic) NSArray<YMKPlacemarkMapObject*>* placemarks;

@end

@implementation ViewController

    static NSString* addressKey = @"address";
    static NSString* addressesKey = @"addresses";
    static NSString* iconKey = @"icon";
    static NSString* selectedIconKey = @"selectedIcon";

    - (void)viewDidLoad {
        [super viewDidLoad];
        _searchManager = [YMKMapKit.sharedInstance createSearchManagerWithSearchManagerType:YMKSearchSearchManagerTypeCombined];
        [_mapView.mapWindow.map addInputListenerWithInputListener:self];
        
        YMKPoint* point = [YMKPoint pointWithLatitude:48.490032 longitude:135.083148];
        YMKCameraPosition* cameraPos = [YMKCameraPosition cameraPositionWithTarget:point zoom:16 azimuth:0 tilt:0];
        [_mapView.mapWindow.map moveWithCameraPosition:cameraPos];
        
        NSDictionary* addMarkerJSONExample = @{
                                      addressKey: @{
                                              @"id": @"dsa21-13cxzas-41sdsa",
                                              @"latitude": @(48.490032),
                                              @"longitude": @(135.083148)
                                              },
                                      iconKey: @"iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAETElEQVRYR+1WXWhcVRCembNud2OyhG1rTMRWKFaUWFCkFPTdFFL1QRQCtoYiwZrc7e6DRJBiqbZqIt69N1rEB8VWxSfRVOvPa9+saE0jCCra2i4pZGPY2G2SvfPJCXvLcsnPjbXmxft2zpk53zcz38y5TGv88Rrj0/8EVp2B4eHhDclkskdVu4wx24IguMmW0RhziYjOADgpIh/29/dPxilvbAJDQ0M3JpPJ55k5R0Tp5S5X1cvGmNeTyeThvr6+y8vZxiLged4WZh4FcGecqBpszgZB8HA+n/91Kb8VCVhwVT0lIjevEjw0LwVB8MBSJJYlYNOeSqW+iUauqheNMb6qnshkMj9bpJmZmdtVtZuZB4ioPUL2bK1W214oFKrRIJYlUCwWjzDzYKMTgA/a29t3l0qlPDP3ENFd9fNxAO+nUil/dnb2GBE9HvE7lMvlDsQmUFf7uUbBWfBEIjGoql8uo4dxIuoiouEIib9EZHO0O5bMgOd5DhEVQ8Y27R0dHZsmJibGYohxvFwu35vNZm0AbeEdzPzMwMDAm41ZWJKA67qfi8jOBufnVLXGzEN2T1WviEihWq0eT6fTDOAJZn6NiNbZc2YuqGoTM78Y3gHgRC6X2xWLgO/7fwC4pcH5bmZ+j4juqe/tY+ZLAI7aNYCnmPlWIvLr598S0V4i+r4B8LzjOJtiEXBdd05EbgiNW1pa0pVK5c8wwmq1mmlqavIBnGbmX4IgeDedTm+dnZ21Nvar1mq19YlEonEQzTmOs5Chq5mNqjJce54XEJGE63K5vC6bzVbq4/aAMea8qj5mu4CZTwVBMGi1YYyZ+lcIuK5bahw+ItKpqsdUtRPAVhGxY3kvgIM29SLyMYAtANy6Rk7bshhjvvtHJfA87wsiejAiwjlmtlkYU9VPRORrETkyPz9/IZFI7AHwKhEl6yLcD6CFiA41EBh1HOehWCXwfT8XRlO/8MLk5ORt2Wz2PgvOzHtE5CcAnUT0aeOlAMYymcz26enp30Vk4bUMhes4zoJo42hgo6qeE5GUNQbQx8w/WDALDsD299tE9CMRbQsvtOAi0gXAzpBHw31VnVHVzYVCoRyLgDUqFouvMPOzzPw0ANtOC+BtbW1flUqlfQB2E1GniEBVx40xx5ubm9+qVCp2FF8FrwdwMJfLvRAV/YqvoXXwPG+HBQfwJBFtFJGXVPUNIhq1j9HU1BQz8x3GmG5VHYiknYIgONPa2rqjt7f3SmwCnuchYtwNYAOAd0QkFvF6N1wEcH8+n/8tCr6grcU2w70wchu14zjsuq6uBtxGTkSPLAW+KIFo5CKyS1Vt+7Dv+4cB7I/xSzZj34VMJvPyYmlfUYTRyC0pS8A6joyMrA+CoAfATvtTWu8GJaKJ+tw/WavVPoqqfalML1oC3/c/Y+ajYeTLlelazxYlAMCqGo2RXyvQqjJwvcBW3QX/BZHY/Xy9yKw5gb8BqzUYPyCLyH8AAAAASUVORK5CYII="
                                      };
        
        [self addMarkerWithJSON: addMarkerJSONExample];
        
        NSDictionary* addMarkersJSONExample = @{
                                      addressesKey: @[
                                              @{
                                                  @"id": @"id1",
                                                  @"latitude": @(48.4870422),
                                                  @"longitude": @(135.0809167)
                                                  },
                                              @{
                                                  @"id": @"id2",
                                                  @"latitude": @(48.4886112),
                                                  @"longitude": @(135.0840405)
                                                  }
                                              ],
                                      iconKey: @"iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAE5klEQVRYR8WXbWxTVRjH/8+5p71d220dL+MtDrZ2CJJIZAMNMQZRFBPR+PLBBMW3QHAmRk00BiUG3/hAjB/ETBJMwBj9QEgWXyJhREmMGgJoIEbIWmAismXjpWNb23vbcx5zb7duLe22bkRPcj/0nuf5P7/znOc595TwPw8qN/61U4umez3p5WAjTEC1489AH6BjdkYeq1p8+nI5mhMC4BO3Bix/6hlmfpoElpcKoDWYiI8SsNdM+PfS0pOD48GMCcAMwzrbuBGs3wXEjPHECuZ7AXrLDHfsJoIu5VsSYOBs/SypjC9BtLrMwPnmjPY0eH1lY6y3mE5RgOTpm+tJZg4BomFKwUecY2wYayrqT3cW6l0H4K5ci19uYPDhmLE088rCTOQBuHseixycctpLpY3Rbkaia0fXRB5A6kzjZjBab1Dai8ow08aKxo7dw5M5ALfVAoOdk6j28ng1eszgYD3NvZhwHHMAqejCF0G8szy1SVoTXvCFo5/mASSjkSNEtGKSkmW5EfCrGYmuzAFkj1d1qSyVKRg7J2aFaU2n+eevuluQioXXAuL7KWiW7ypwn68h2p4F+C/3P1f+3OILx1pdACu6cAsTvz88p+KDcJ6xhndBbd603dkzpr0RCsB53KE1iIwtZmPH9qIAjo1R9QS+ONCEr7857Po88vBqPLV+HTjdDfv8XUWDJaoPouWVVliWDZ/pRevOraiuDiLT+ybUtX05Hz2QghGoGAEougWiCmLut/jxp3MQRLh71XIYhoF013vQic+LAojAc7hobcCx3/5Ec9MSzK+bA870wP77AUD3uz6sNPSVfhi11W4rDtVA5H4QHShUJU8D5MytEL4VYHUF6vJnyFzeDTJlUQC2FOSMTTCmPQsyaqCTR5C59A44PfINUtcS4KQNObtmjS/cccgFcNpQGqpXiJGDSSdsEAHkMcDO63QGcAxKBB8m4lQa7Hh4pCvGSsG5DZDPA7ZsqHgCmokr5/mnUX1nPHcSFjuIOKNAGQ1mBqRwRccdDLADqxQcN0jDXQTc1A+4WizpQuXtPTflDqJsK0ZaQPTJuAEmYcBpDdU34EI4Q/jlR/7bul7NA8h+jJLnAMycSIy+AYlTZ/wgMBaHE6gKquJ1YWeg+xJgnQ2uBVKVNVxLiy65VZn/OY4t3ATwrrEAtCbsaZuFr76rRUZl3T2SsX5dDzY81O3WTbbcAZ204LRcdi+Ghs/YEWzqfn34Z+GFRKT+WPAzfPIOyinl4+xtm4U9bbPdl+aQjTUU4PnHu/Dkgz3Qzqr7k0AmPyssZTS4omtRyQuJIxo/WtcglXVceD0h8ntB0sgRDCQMPPrSEphK4LWQH3eaXjeHh1M2PnSq26Owb9sR+IR9XRLZEPG0DCyd1nz2/OjJopfS+Mm6JqPf+oGgq9gQEF7pgkQvBLH5g1uwJRTAPRVeN83DiWpP2tgeH8Sul4+hYU7+Me4EJ69cFVz2z4lCspLX8uSJ+fWZpHWIlJrazVjKqG1U3Fu48qI1UEjHDDF4fN4OTqdbSLNv9LxO5qdZOBkZNZxqF17j48Cy7jcm9cdktBj/Hgol2b9N2eoxaD2PRhV1np2TT0kXSHj2B5B4m5qv9o3VUde14XjGbnf9VVdjKbMZzBHSIuS+E3wVoJgpU8edW85EdCa0BeUITdb2X3Um/TDyJQTFAAAAAElFTkSuQmCC",
                                      selectedIconKey: @"iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAF8klEQVRYR8WXeWwUVRzHv7/3Znb26patBWKxlra7RcIlWlCiwYsomkg0MfEPI9EEKWpiguKF8IcmgKjVRI2GQxMl+ofRaDQaDwiieIAIVu62W5QecrS03W53dnZn3jMztAuzRwupie+fzc77HZ/3+733nTeE/3nQxeaPH60r93CrHuARggzb/lLKHgCtmlfbQ5UHz1xMzAsCkE0zA0Yg9aCU4n4paQ5jKOgnBARn2C1Jvq/5ku9RRVdyNJgRAaQEGW2RZZDyBYCVjxbMNS9wCoxWa7XNm4ggi/kWBRhorZ2gCvYhGG65qMS5xoRv0ybdF5rS3F0oTkEA/ciUalIz2yB59ZiSDzkLgRbi1gJfbdvxfL6cJwMtkfEqE7v+q+TD4W0I0+LzQlOP2Bs2O1wVcHreEv1uzGUvXrZvvJGWhUUBUrHoMki8PZayxxMcoaBVNISUcokv2vrOsEG2AmePmn4MwPixAByKBZAyCOXjMrgkbCLgy4Gx6ITmT9ZQZYdu58kCpGKRhyHprbEkH/ZNZxi6exX09KoQIEyaYKA8nMmGlqClvkjzJheA0Rr9WQLzhq0sAcQHVAwMMgzqCuKDDOPLMqi5LPUfMIofvZHY/CyA/PvysG5oPecr3NG//LbGoiQgEPSbzi9RUT0ZgmLg4YfASxeDWBlEajfM089DZtrcGiUgfCTDFG2NOy1ItURuA9HXBYVCnQwWuA3S7IJIfAWg+Abj4QYoZU/gtz0H0NF5CjfdOAelwRTSx28HRNwVXhIt8NU2bxsCqHsUJN/MEwl1MmjiJ9i240/URatQVbYDZvfzRVvgmfwT9uw7iXUvnd3ktbWVeHnd4zBPr4IV/yjHjxq8keaNDoDRGl0tgRdyI/NxDdj6+7XYuPljlJWVYvPbz8Jom1EUQKs5jE8/34EtH3zh2Hg1Dz7csh7mmUZYvRvObUIhwYit1KLN60YEYME70Z5cjrXrN6P+6mloeOBqpI+7dMQFo1a8h3h6Ota8uAmdHSex+P5FWHjrdch03guR2pe1FQkDPKg9rUVaXhqxBQCHUr4KPHQPpNmJzMknIY39RStAag3USVtA/JyUWH3vwOxZ71q91R0HnzDOOYqjbsKspyUghYBMpUFeDaRyN4gUsFcGXxhq+A5AGQ+p73at3HYQ8SSEngafGLrZF4ltdwBk+7QyXU93F7toOI4DOqAwMI8KyRgOxwJoag44EFdOSeCKmiSIAJE0IDMWeKk/v1JGGmZfEkJCllxaEqbatv6sEuYKUbE6pwyGNRuqsHNvyGUyv74fK5ceh+YRhV1NC+aZhKMtQqGO0DWnKrNC5GjBBb6IXnm3El/+UFYwyaKbe7B8cUf+nCmQ6U2AxBCc5nk1WN/5hAtAdlX4jUTgGBgmFFu9/aa7+7FpEKLwRYoxiU9fP+h6G8q0BREfhLS13W4lg16impdSfW+/C8CpQmvdUkCeO7A5JPubA3hsbaToKbAn3niuBdOjSdi3QKEbEImUU/bsUNXG4NyuFcP/8y4kqQOTf4ZXuZbsHZUz/jgSxPIXa0cEeO2ZGGZW90EkdMB0yzYpvNU/90Td+ZfUvCx9u6qquUjt45paSn4PSDl33IYBPARcxjmUIUhTSnRYFtISaHxkP2ZW5X8aSLB+rvIZ/rld7eevoGAzB5uqrhKDqe0kRUhy++gpDkhTLITHG69ApcLgI+aqhC4F2k2BVxqaMKumzzUnOfWR4pkfrO/IU7Gi13K9qara1I2tZFk1w9Ga2sZhxYZZmKd5cFdAw1TP2eocSpv4LGHg13QmH0BRWjj3LfDV59+I8zZhbnOlBNP3Tmq0jPQykvDaAI2bZ2NjeQhB5mYfkBJLTsfx1JJ9TgUEUQoqfys05x/nuBUbF/ZpFqspHTyj/5KMY+rOLdegTi8pGO+IL44bFu+Cx4uDoYn+62ylG3HHnn8nHM1w8LeK2SJtbSeI0hFtifVL1XN9yZz2A6PFHLUFeS3ZfqOSqWyfZRH3OXMEginPVpEkcc509e+KvXTT9+aFJLdt/gVMYnA/pLeL4QAAAABJRU5ErkJggg=="
                                      };
        [self addMarkersWithJSON: addMarkersJSONExample];
        
        [self.view bringSubviewToFront:_actionView];
    }

    - (IBAction)toggleUserLocationButtonTapped:(UIButton *)sender {
        NSString* userIconExample = @"iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAACUklEQVRYR2NkGGDAOMD2M5DjADYDe6/NDExM/CiO/8f48cLBzb4MDAy/SPEUyQ4wsbLvve66qOg7lySKPZzfnjNo7o7pO3PscDFNHWBmad933WVO4Wc+FRR7eD7dZdDclzLh9NEDhaMOoH0IuM4p/MyPJQr20CsK3HA4YDe9HOCOwwG76OUATywO+HiXQXMnvRzghcMBO+jpAAG0RAgKge30coDPnMLP2BywlV4O8MXigA93GTTp5gA/HA7YQq8Q8J9T+FkQLQ2AQmATvRwQgMMBG+nhAGv7vuvYQuD9XQbNzSkTTh+mcm1obm4e8p+FwxBWwfxg5Te8ErXa8x8zG0qdw/T3F4POstDtHL8/nodJMDP+PXP8yJH1+ConvA0SA1OrhLfmyTM/ylvDbfsioMiAbjnMApAjeD7ch9vH9+jQD7HTC1POnTi6FJcj8DrA0Mqp527QtOJPwuqk1LAIB7y5waC4Nr3z4olDFWQ5wNPTk/0Rk8jRq2GLjMlxgcaK6NOKDO9tt2/f/pMsB4A0OTg4cLznEj91MWKFLimOMFwSflmF/7/J6tWr8TZSiWqUOjg48LznEj92MZI4RxguDr8sw/7dYvPmzd8IOZooB4AMcXNz437JInj8YjR+RxguJN5ykLlEOwDmiNcsgsfPx2B3hOEC0iwn2QEgDb6+vlxPfnKdOJ+I6gjD+aRbTpYDYI64/19w70OPagsQX35H6wlFxvfOxMQ5epogKQqQNXt6evI9//Kzh4GR6b8kN2vp9u3bPxFKcNjkyXYAOZYNSgcAACfrPjD5GlhGAAAAAElFTkSuQmCC";
        [self showUserLocation: userIconExample];
    }

    - (IBAction)moveToUserLocation:(UIButton *)sender {
        [self moveToUserLocation];
    }

    //MARK: Реализация функции onMapTapListener(address => address)
    - (void)onMapTapWithMap:(YMKMap *)map point:(YMKPoint *)point {
        YMKSearchOptions* options = [YMKSearchOptions new];
        options.searchTypes = YMKSearchTypeGeo;
        
        _searchSession = [_searchManager submitWithPoint:point
                                                    zoom:@(18)
                                           searchOptions:options
                                         responseHandler:^(YMKSearchResponse *response, NSError *error) {
                                             NSMutableArray* addressArray = [NSMutableArray new];
                                             for (YMKGeoObjectCollectionItem* item in response.collection.children) {
                                                 [addressArray addObject:item.obj.name];
                                             }
                                             id errorInfo = [NSNull null];
                                             if (error != nil) { errorInfo = error.localizedDescription; }
                                             
                                             NSDictionary* addressDict = @{
                                                                           addressKey: response.collection.children.firstObject.obj.name,
                                                                           addressesKey: addressArray,
                                                                           @"error": errorInfo
                                                                           };
                                             NSLog(@"onMapTapped with output info: %@", addressDict);
        }];
    }

    - (void)onMapLongTapWithMap:(nullable YMKMap *)map
                          point:(nonnull YMKPoint *)point {
        [self onMapTapWithMap:map point:point];
    }

    //MARK: Реализация функции showGeoObject(point: Point, icon)
    - (void)addMarkerWithJSON: (id)json {
        NSDictionary* dict = nil;
        if ([json isKindOfClass:[NSDictionary class]]) {
            dict = (NSDictionary*)json;
        }
        WSPoint* customPoint = [[WSPoint alloc] initWithJSON:dict[addressKey]];
        customPoint.icon =  [dict[iconKey] decodeBase64ToImage];
        if (customPoint == nil) { return; }
        
        YMKMapObjectCollection* mapObjects = _mapView.mapWindow.map.mapObjects;
        [mapObjects clear];
        YMKPoint* point = [YMKPoint pointWithLatitude:customPoint.latitude longitude:customPoint.longitude];
        YMKPlacemarkMapObject* placemark = [mapObjects addPlacemarkWithPoint:point];

        [placemark setIconWithImage: customPoint.icon];
    }

    //MARK: Реализация функции showGeoObjectsCollection(points: Array, icon, selectedIcon, onTapListener: id => id)
    -(void)addMarkersWithJSON: (id)json {
        NSDictionary* dict = nil;
        if ([json isKindOfClass:[NSDictionary class]]) {
            dict = (NSDictionary*)json;
        }
        
        NSMutableArray<YMKPlacemarkMapObject*>* placemarks = [NSMutableArray new];
        YMKMapObjectCollection* mapObjects = _mapView.mapWindow.map.mapObjects;
        [mapObjects clear];
        
        for (NSDictionary* addressDict in dict[addressesKey]) {
            WSPoint* customPoint = [[WSPoint alloc] initWithJSON: addressDict];
            if (customPoint != nil) {
                customPoint.icon = [dict[iconKey] decodeBase64ToImage];
                customPoint.selectedIcon = [dict[selectedIconKey] decodeBase64ToImage];
                
                YMKPoint* point = [YMKPoint pointWithLatitude:customPoint.latitude longitude:customPoint.longitude];
                YMKPlacemarkMapObject* placemark = [mapObjects addPlacemarkWithPoint:point];
                [placemark setIconWithImage: customPoint.icon];
                [placemark addTapListenerWithTapListener:self];
                placemark.userData = customPoint;
                [placemarks addObject: placemark];
            }
        }
        self.placemarks = [NSArray arrayWithArray: placemarks];
    }

    //Реализация тапа по геометке
    - (BOOL)onMapObjectTapWithMapObject:(YMKMapObject *)mapObject point:(YMKPoint *)point {
        if ([mapObject isKindOfClass:[YMKPlacemarkMapObject class]] && [mapObject.userData isKindOfClass: [WSPoint class]]) {
            YMKPlacemarkMapObject* placeMark = (YMKPlacemarkMapObject*) mapObject;
            WSPoint* customPoint = (WSPoint*) mapObject.userData;
            
            //Снимаем все метки, кроме выбранной
            for (YMKPlacemarkMapObject* item in _placemarks) {
                if (item != placeMark) { [item setIconWithImage: customPoint.icon]; }
            }
            
            [placeMark setIconWithImage: customPoint.selectedIcon];
            NSLog(@"onMapObjectTapWithMapObject with point identifier: %@", customPoint.identifier);
        }
        
        return true;
    }

    //MARK: Реализация функции moveToUserLocation
    - (void)moveToUserLocation {
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        //Пользователь предоставил свои данные о местоположении
        if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
            YMKPoint* lastUserLocation = _mapView.mapWindow.map.userLocationLayer.cameraPosition.target;
            YMKCameraPosition* cameraPos = [YMKCameraPosition cameraPositionWithTarget:lastUserLocation zoom:12 azimuth:0 tilt:0];
            [_mapView.mapWindow.map moveWithCameraPosition:cameraPos];
        }
    }

    //MARK: Реализация функции showUserLocation(icon)
    -(void)showUserLocation: (NSString*) icon {
        //Пользователь запретил определение локации, нужно отправить onLocationFail
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
            NSLog(@"locationManager authorizationStatus: kCLAuthorizationStatusDenied");
        }
        //Еще не производилась настройка определения местоположения
        else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
            self.locationManager = [CLLocationManager new];
            self.locationManager.delegate = self;
            [self.locationManager requestWhenInUseAuthorization];
        }
        else {
            YMKUserLocationLayer* userLocationLayer = _mapView.mapWindow.map.userLocationLayer;
            _userLocationIcon = [icon decodeBase64ToImage];
            [userLocationLayer setEnabled: true];
            [userLocationLayer setHeadingEnabled: true];
            [userLocationLayer setObjectListenerWithObjectListener: self];
        }
    }

    - (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
        //Пользователь решил не давать доступ к своей локации, необходимо вызвать метод onLocationFail
        if (status == kCLAuthorizationStatusDenied) {
            NSLog(@"locationManager didChangeAuthorizationStatus: kCLAuthorizationStatusDenied");
        }
        //Пользователь предоставил доступ к своей локации, необходимо вызвать метод onLocationSuccess
        else if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
            NSLog(@"locationManager didChangeAuthorizationStatus:  kCLAuthorizationStatusAuthorizedAlways");
        }
    }

    - (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
        //Произошла ошибка при попытке получить доступ к локации пользователя, необходимо вызвать метод onLocationFail
        NSLog(@"locationManager didFailWithError: %@", error.localizedDescription);
    }

    //MARK: YMKUserLocationObjectListener delegate
    - (void)onObjectAddedWithView:(YMKUserLocationView *)view {
        [view.pin setIconWithImage: _userLocationIcon];
        [view.arrow setIconWithImage: _userLocationIcon];
        view.accuracyCircle.fillColor = UIColor.cyanColor;
    }

    -(void)onObjectUpdatedWithView:(YMKUserLocationView *)view event:(YMKObjectEvent *)event { }
    -(void)onObjectRemovedWithView:(YMKUserLocationView *)view {  }

@end
