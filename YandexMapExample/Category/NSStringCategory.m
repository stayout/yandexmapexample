//
//  NSString+NSStringCategory.m
//  YandexMapExample
//
//  Created by Stayout on 05.04.2018.
//  Copyright © 2018 WS. All rights reserved.
//

#import "NSStringCategory.h"

@implementation NSString (NSStringCategory)

-(UIImage *)decodeBase64ToImage {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:self options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}

@end
