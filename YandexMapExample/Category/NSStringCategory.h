//
//  NSString+NSStringCategory.h
//  YandexMapExample
//
//  Created by Stayout on 05.04.2018.
//  Copyright © 2018 WS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (NSStringCategory)

-(UIImage *)decodeBase64ToImage;

@end
