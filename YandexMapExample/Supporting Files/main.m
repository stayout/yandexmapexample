//
//  main.m
//  YandexMapExample
//
//  Created by Stayout on 04.04.2018.
//  Copyright © 2018 WS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
