//
//  AppDelegate.m
//  YandexMapExample
//
//  Created by Stayout on 04.04.2018.
//  Copyright © 2018 WS. All rights reserved.
//

#import "AppDelegate.h"
#import <YandexMapKit/YMKMapKitFactory.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [YMKMapKit setApiKey: @"8c5dcbd0-f43f-478f-bcfd-28f37fc8b103"];
    return YES;
}

@end
