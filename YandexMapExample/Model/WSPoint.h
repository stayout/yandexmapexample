//
//  WSPoint.h
//  YandexMapExample
//
//  Created by Stayout on 05.04.2018.
//  Copyright © 2018 WS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface WSPoint : NSObject

@property (nonatomic) NSString* identifier;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;
@property (nonatomic) UIImage* icon;
@property (nonatomic, nullable) UIImage* selectedIcon;

-(instancetype)initWithJSON:(id) json;

@end
